# Gráfico de Pontos

Nosso primeiro objeto de estudo serão os gráficos de pontos (*scatterplot*). Em R, este é o tipo mais simples de gráfico, no qual apresentamos a relação entre duas variáveis como pontos no gráfico. Fazemos um plot básico (um *scatterplot*) com a função `plot()`. Esta é a função mais básica de gráficos em R puro. Sua sintaxe é:

```{r, eval=FALSE}
plot(x, y, ...)
```

Onde:

- `x`: coordenadas dos pontos no eixo X.
- `y`: coordenadas dos pontos no eixo Y.
- `...`: argumentos de parâmetros gráficos.

Os parâmetros gráficos são diversos argumentos que modificam a estética do gráfico. Ao longo desta seção veremos vários deles.

Façamos um plot simples:

```{r, comment=""}
dados_x <- c(1, 3, 4)
dados_y <- c(4, 5, 8)

plot(dados_x, dados_y)
```

Acima, colocamos as coordenadas de cada eixo em um vetor e colocamos ambos vetores como argumentos para a função `plot()`. Se perceberem, no gráfico, os pontos, respectivamente, estão nas coordenadas: `(1, 4)`; `(3, 5)`; e `(4, 8)`. Estão são exatamentes as coordenadas que passamos em cada vetor.

Vamos utilizar um conjunto de dados maior:

```{r, comment=""}
data(iris)
```

Em R, é muito comum vermos gráficos (e algumas análises) sendo feitos com a função `with()`. Sua sintaxe é:

```{r, eval=FALSE}
with(data_frame, expressão)
```

Esta é utilizada para avaliar expressões/funções de acordo com algum conjunto de dados (apenas no formato data frame!). Veja um exemplo:

```{r, comment=""}
# Data frame:
valor <- c(12.5, 50.4, 34.9, 9.8)
desconto <- c(5, 20, 12, 2)

dados <- data.frame(valor, desconto)

# With:
with(dados, valor * (desconto / 100))
```

Neste exemplo de `with()`, calculamos o valor de descontos. Na expressão: `with(dados, valor * (desconto / 100))` queremos dizer: "com o conjunto de dados `dados`, avalie a expressão `valor * (desconto / 100)`".

Como as funções de gráfico em R puro geralmente não possuem um argumento `data` para especificar de onde os dados do gráfico devem ser retirados, utilizamos `with()` para desempenhar este papel.

```{r, comment=""}
with(iris, plot(Petal.Length, Petal.Width))
```

Acima, dizemos que quermos fazer um plot com `Petal.Length` no eixo X e `Petal.Width` no eixo Y, tirando estes dados de `iris`.

Uma outra forma também utilziada de plots básicos é simplesmente indexar os dados desejados:

```{r, comment:}
plot(iris$Petal.Length, iris$Petal.Width)
```

Por fim, necessitamos apenas especificar quais dados devem estar em qual eixo.

## Tipos de Pontos

Agora que sabemos como fazer um gráfico, vamos começar a brincar com os parâmetros gráficos; com a estética dos gráficos. Iniciaremos pelos símbolos dos pontos.

Os pontos que vimos são os pontos default de `plot()`, mas existem diversos outros símbolos para os pontos. Especificamente, 25 símbolos:

```{r, echo=FALSE}
grid <- expand.grid(1:5, 6:1)

plot(
    grid,
    pch = 0:30,
    cex = 2.5,
    yaxt = "n",
    xaxt = "n",
    ann = FALSE,
    xlim = c(0.5, 5.25),
    ylim = c(0.5, 6.5),
    bg="grey"
)

grid2 <- expand.grid(seq(0.6, 4.6, 1), 6:1)

text(grid2$Var1[1:26], grid2$Var2[1:26], 0:25)
```

Algumas diferenças entre os símbolos:

- Símbolos `0-14`: símbolos "ocos"; sem preenchimento.
- Símbolos `15-20`: símbolos com preenchimento (default: cor preta).
- Símbolos `21-25`: símbolos com borda e preenchimento separados.

Vamos utilizar alguns destes símbolos:

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch=0)
```



```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch=1)
```
> Nota: `pch=1` é o argumento default de `plot()`.

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch=5)
```

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch=11)
```

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch=16)
```

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch=18)
```

### Outros Tipos de Símbolos

Além dos 26 símbolos default anteriores, podemos especificar muitos outros símbolos por seus valores ASCII. Contudo, é muito mais simples colocá-los como strings. Alguns exemplos abaixo:

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch="+")
```

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch="!")
```

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch="&")
```

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch="$")
```

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch="@")
```

## Cores

Nossa próxima personalização de gráficos será a especificação de cores dos símbolos. 

Em R, podemos especificar cores de diferentes formas com o argumento `col`. Vamos exemplificar todas estas formas ao fazermos os gráficos.

### Cores por Nomes

O modo mais simples de se colocar cores em R é simplesmente colocar uma string com o nome da cor. Com o comando abaixo, podemos ver todos os nomes de cores disponíveis no R:

```{r, eval=FALSE}
colors()
```

Vamos exemplificar algumas:

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=4,
    col="violet"
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=19,
    col="skyblue"
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=19,
    col="royalblue"
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=19,
    col="turquoise"
)
```

### Cores Hexadecimais

Podemos especificar cores através de seus valores hexadecimais. Estes valores seguem o padrão: `#RRGGBB`, onde `R` é vermelho (*red*); `G` é verde (*green*); e `B` é azul (*blue*). A partir da mistura destas cores, pode-se construir todas as outras. O padrão `#RRGGBB` é exatamente esta mistura. Alguns exemplos:

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=19,
    col="#05FB89"
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=19,
    col="#F9D7AA"
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=19,
    col="#875641"
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=19,
    col="#FBACDF"
)
```

Ao invés de colocarmos exatamente o valor hexadecimal, podemos defini-lo através da função `rgb()`. Esta função retorna um valor hexadecimal de acordo com os valores RGB fornecidos. Alguns exemplos com esta função:

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=19,
    col=rgb(10, 20, 255, maxColorValue=255)
)
```

> Nota: usamos o argumento `maxColorValue=255` para especificar que os valores RGB inseridos na função estão no intervalo `0-255`. Caso contrário, o valor máximo, por default, é `1`.

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=19,
    col=rgb(58, 200, 102, maxColorValue=255)
)
```

::: {.infobox .exclamacao data-latex="{exclamacao}"}
**Tente!**<br><br>
A função `rgb()` possui um argumento `alpha`, o qual especifica a quantidade de transparência da cor, sendo `0` completamente transparte e `1` sem transparência alguma. Tente algumas cores em RGB com o argumento `alpha`!
:::

Agora que você conhece a função `rgb()`, há outras funções de coloração em R. Pesquise e as utilize também: `rainbow()`, `hsv()`, `hcl()`, `gray()`.

### Cores por Números

Podemos também especificar cores pelos números de `1-8`. Estes números correspondem ao índice de um vetor de palete, que possui oito cores:

```{r, comment=""}
palette()
```

Alguns exemplos:

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch=19, col=1)
```

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch=19, col=2)
```

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch=19, col=4)
```

```{r, comment=""}
plot(iris$Petal.Length, iris$Petal.Width, pch=19, col=8)
```

## Preenchimento

Com os símbolos `21-25`, podemos ter cor de preenchimento e cor de borda separadamente. Isto porque estes símbolos possuem background e borda independentes. O preenchimento do símbolo é feito com o argumento `bg` (*background*), enquanto a coloração da borda é feita com `col`.

Nossa próxima personalização de gráficos será a especificação de cores e preenchimento dos símbolos. 

Em R, podemos especificar cores de diferentes formas com o argumento `col`. Vamos exemplificar todas estas formas ao fazermos os gráficos.

Um **detalhe importante**: lembremos da seção inicial que nem todos símbolos podem ter preenchimento. Tenha atenção em quais símbolos você está trabalhando.

Vejamos alguns exemplos:

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=21,
    col="red",
    bg="gray"
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=22,
    col="navyblue",
    bg="#FF25BC"
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=23,
    col="#ACDDDC",
    bg="yellow"
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=24,
    col="violet",
    bg="tomato"
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=25,
    col="seashell",
    bg="orchid"
)
```

## Tamanho

Outra personalização de gráficos: podemos especificar o tamanho do símbolo utilizado com o argumento `cex`: um argumento numérico representando a quantidade com a qual o símbolo será aumentado, relativo ao valor default (`1`).

Exemplos:

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=21,
    col="red",
    bg="gray",
    cex=2
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=22,
    col="navyblue",
    bg="#FF25BC",
    cex=4
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=23,
    col="#ACDDDC",
    bg="yellow",
    cex=6
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=24,
    col="violet",
    bg="tomato",
    cex=10
)
```

Além do tamanho do símbolo, podemos aumentar também, com o argumento `lwd`, a espessura da borda:

> Nota: veremos adiante outras funções do argumento `lwd`. Aqui, basta sabermos que a borda é uma linha, então mudamos sua espessura com *line width* (lwd).

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=21,
    col="red",
    bg="gray",
    cex=2,
    lwd=2
)
```

```{r, comment=""}
plot(
    iris$Petal.Length,
    iris$Petal.Width,
    pch=23,
    col="#ACDDDC",
    bg="yellow",
    cex=6,
    lwd=6
)
```