# For

Aqui entramos nas estruturas explícitas de repetição. A primeira destas que veremos é o `for`. O loop `for` é uma estrutura de repetição controlada por contagem, ou seja, sabemos e definimos exatamente quantas iterações haverão ao total. Sua sintaxe é:

```{r, eval=FALSE}
for(variavel in obj){
    bloco
}
```

Onde:

- `variavel`: variável criada no loop que tomará os diferentes valores de `obj` a cada iteração.
- `obj`: vetor ou lista sobre o qual aplicaremos o processo iterativo.
- `bloco`: bloco de comandos que serão executados a cada iteração.

O fluxo de um loop `for` é da seguinte forma:

```{r for_loop, echo=FALSE, fig.align = "center", fig.cap="Fluxo do loop for."}
knitr::include_graphics("img/for.png")
```

Em palavras:

1. O loop tem início e a variável para fazer as iterações é criada.
1. A primeira iteração tem início. Nesta, a variável tem o valor do primeiro elemento do objeto a ser iterado.
1. O bloco do loop é executado.
1. Verificação: este foi o último elemento do objeto?
    1. Se sim, o loop é encerrado e o programa continua.
    1. Se não, o `for` volta para o início e inicia-se a próxima iteração, onde a variável toma o valor do próximo elemento do objeto e o passo 3 é executado.
    
A repetição do loop `for` é feita nos passos 3 e 4, até que o último elemento do objeto seja iterado e o loop tenha fim. Nas próximas seções, vamos criar vários exemplos deste loop e esclarecer na prática nossas dúvidas.

## Criando loops `for`

Vamos começar por um exemplo bem simples. Vamos iterar sobre um conjunto pequeno de letras.

```{r, comment=""}
# Vetor para iteração:
vogais <- c("a", "e", "i", "o", "u")

# Loop:
for(letra in vogais){
    print(letra)
}
```

Neste exemplo, nosso objeto a ser iterado é o vetor `vogais` e nossa variável é `letra`. Falamos que `for` é loop controlado por contagem, ou seja, sabemos **antes de iniciar o loop** quantas iterações haverão. Neste exemplo, sabemos que ocorrerão cinco iterações no total, pois o vetor `vogais` sobre o qual estamos iterando possui cinco elementos.

O loop acima ocorre da seguinte forma:

1. Início do loop.
1. Primeira iteração: a variável `letra` é criada e o primeiro elemento do objeto (a letra `"a"`) é atribuída a ela.
1. É feito o print da variável `letra` (letra `"a"`).
    1. É o último elemento de `vogais`?
    1. Não, então volte ao início.
1. Segunda iteração: o segundo elemento da variável `vogais` (letra `"e"`) é atribuído a `letra`.
1. É feito o print da variável `letra` (letra `"e"`).
    1. É o último elemento de `vogais`?
    1. Não, então volte ao início.
1. Terceira iteração: o terceiro elemento da variável `vogais` (letra `"i"`) é atribuído a `letra`.
1. É feito o print da variável `letra` (letra `"i"`).
    1. É o último elemento de `vogais`?
    1. Não, então volte ao início.
1. Quarta iteração: o quarto elemento da variável `vogais` (letra `"o"`) é atribuído a `letra`.
1. É feito o print da variável `letra` (letra `"o"`).
    1. É o último elemento de `vogais`?
    1. Não, então volte ao início.
1. Quinta iteração: o quinto elemento da variável `vogais` (letra `"u"`) é atribuído a `letra`.
1. É feito o print da variável `letra` (letra `"u"`).
    1. É o último elemento de `vogais`?
    1. Sim, então encerre o loop.

Vamos iterar sobre um objeto maior: o vetor de caracteres `letters` (próprio do R).

```{r, comment=""}
for(letra in letters){
    print(letra)
}
```

Neste exemplo, nosso objeto a ser iterado é o vetor `letters`; nossa variável é `letra`; e o total de iterações é 26, pois `letters` tem 26 elementos. Todo o loop é feito analogamente como explicitamos detalhadamente no exemplo anterior, das vogais.

> Nota: um detalhe importante em programação — todas as nomenclaturas devem ser claras e precisas, sejam estas nomes de variáveis, funções, scripts, etc. Já falamos um pouco sobre isso em variáveis, mas sempre é bom relembrar. Evite nomear variáveis (em loops ou não) como `x`, `y` ou afins. Coloque sempre nomes que reflitam o que o objeto contém; ou o que está sendo feito ou avaliado. Por isso nossas variáveis nos loops acima chamavam-se `letra` pois, a cada iteração, elas tomam o valor de alguma letra do objeto.

Vamos iterar sobre um vetor numérico:

```{r, comment=""}
for(numero in 0:10){
    print(numero)
}
```

Nosso objeto agora é o vetor `0:10` (portanto, o total de iterações é 11) e iteramos sobre ele com a variável `numero`, a qual toma um de seus valores a cada iteração.

Loops também aceitam qualquer tipo de vetor válido:

```{r, comment=""}
for(numero in seq(0, 50, 10)){
    print(numero)
}
```

Acima, temos a função `seq()` que retorna o vetor `c(0, 10, 20, 30, 40, 50)`. Este vetor é então usado para a iteração.

Um detalhe é que, assim como a estrutura condicional, o loop `for` não precisa possuir as chaves:

```{r, comment=""}
for(numero in 0:10)
    print(numero)
```

As chaves agrupam um conjunto de códigos e cria assim um bloco de códigos. Se não há vários códigos a serem executados, as chaves não são obrigatórias. Lembre-se da legibilidade: se não quiser utilizar chaves, separe bem as linhas de seu código para que não haja confusão sobre o que pertence ou não ao loop (na verdade, utilize este pensamento sempre em qualquer situação).

Um último detalhe sobre as variáveis utilizadas no loop `for`: estas variáveis são criadas no início do loop, mas não são removidas da memória após o término do loop. Veja em seu ambiente que as variáveis `letra` e `numero` ainda existem em seu ambiente no R. Quanto ao seu valor, sabemos que, a cada iteração, estas variáveis trocam seu valor de acordo com o elemento do objeto. Desta forma, seu valor ao término do loop será o último valor sobre o qual elas iteraram. 

## `for` aninhado

Podemos aninhar um loop `for` em outro, isto é, colocar loops independentes um dentro de outro. A sintaxe de um único `for` aninhado seria:

```{r, eval=FALSE}
for(variavel1 in obj){ # loop externo
    
    bloco1
    
    for(variavel2 in obj){ # loop interno
        bloco2
    }
}
```

Na sintaxe, é perceptível que há dois loops independentes: o loop externo e o loop interno. Também vimos que há duas variáveis de iteração diferentes: `variavel1` e `variavel2`. Estas variáveis são diferentes justamente porque pertencem a loops diferentes — uma delas pertence ao loop externo (`variavel1`) e a outra ao loop interno (`variavel2`).

O loop interno faz parte do `bloco1`, isto é, o bloco de código do loop externo. Em outras palavras: o loop interno está dentro e é executado pelo externo. Se não houver iteração no loop externo, o loop interno nem ao menos se iniciará.

Vamos entender vendo na prática. Para facilitar, vamos usar um vetor numérico no loop externo e um vetor de caracteres no loop interno:

```{r, eval=FALSE}
for(numero in 1:5){ # Loop externo
    
    for(letra in c("a", "e", "i", "o", "u")){ # Loop interno
        
        print(paste(numero, letra))
    }
}
```

No loop externo, estamos iterando sobre os número de 1 a 5 e no loop interno, estamos iterando sobre as vogais. Dentro do bloco do loop interno, fazemos um print da função `paste()`, que retorna os valores de `numero` e `letra` a cada iteração.

Vejamos o retorno deste loop:

```{r, comment=""}
for(numero in 1:5){ # Loop externo
    
    for(letra in c("a", "e", "i", "o", "u")){ # Loop interno
        
        print(paste(numero, letra))
    }
}
```

Podemos perceber o padrão de repetição de ambos loops nos prints feitos. Olhe apenas para a coluna dos números: cada número é repetido cinco vezes. Agora veja a coluna das vogais: todas são repetidas em séries de cinco. Este padrão de repetição reflete cada um dos loops — a cada iteração do loop externo, há cinco iterações do loop interno. A figura abaixo demonstra melhor o que está ocorrendo.

```{r nested_for_loop, echo=FALSE, fig.align = "center", fig.cap="Loop aninhado."}
knitr::include_graphics("img/nested_for.png")
```

As iterações do loop interno fazem parte do loop externo, por isto que, em cada iteração do externo, todas iterações do interno são feitas. Veja na imagem acima que o loop externo só vai para a próxima iteração quando todas as iterações do interno forem executadas. Basicamente, o ciclo é:

1. Loop externo é iniciado.
    1. Loop interno é iniciado.
    1. Todas as iterações do loop interno são executadas.
1. Próxima iteração do loop externo.

O `for` só termina quando as iterações do loop externo acabarem. Até lá, o loop interno será repetido inteiramente a cada iteração do loop externo. E se tivéssemos um loop com mais um nível de aninhamento, como abaixo?

```{r, comment=""}
for(numero in 1:5){ # Loop externo
    for(letra in c("a", "e", "i", "o", "u")){ # Primeiro loop interno
        for(palavra in c("muitas", "repetições")){ # Segundo loop interno
            print(paste(numero, letra, palavra))
        }
    }
}
```

O fluxo seria, então:

1. Loop externo é iniciado.
    1. Primeiro loop interno é iniciado.
        1. Segundo loop interno é iniciado.
        1. Todas as iterações do segundo loop interno são executadas.
    1. Próxima iteração do primeiro loop interno.
1. Próxima iteração do loop externo.

Neste caso, todos os elementos do segundo loop interno seriam repetidos a cada iteração do primeiro loop interno, o qual, por sua vez, é repetido totalmente a cada iteração do loop externo. Já ficou um pouco mais confuso, não é? Você consegue perceber que aninhamento ficam difíceis com facilidade? Não somente para nós entendermos e os escrevermos, mas computacionalmente. Dependendo do que estiver manipulando, o processamento de dados aninhados é cada vez mais difícil e complica muito mais se houver vários aninhamentos.

A dica sempre é: tente resolver seu problema sem aninhamentos ou com o mínimo possível. Em R, temos a solução de vetorização, algo que veremos em uma parte adiante deste livro.

## Exercícios

::: {.infobox .exclamacao data-latex="{exclamacao}"}
**Pesquise!**<br><br>
Uma das características de um programador é saber procurar respostas para os problemas em seu código e afins. Não sabe como responder alguma das questões? Pesquise sobre o tema em questão (na internet, na documentação oficial, ...) e aprenda mais sobre ele!
:::

### Questão 1 {-}

Crie um loop `for` que faça o print dos números ímpares de 1 a 1000.

### Questão 2 {-}

Crie um loop `for` que faça o print dos números pares e divisíveis por 8 de 1 a 1000.

### Questão 3 {-}

Faça a tabuada do 5 com o loop `for`.

### Questão 4 {-}

Faça a tabuada de 1 a 10 com o loop `for`.

> Nota: não faça 10 loops diferentes.

### Questão 5 {-}

Faça uma iteração com o loop `for` sobre os valores pares de 500 números aleatórios entre `0` e `1000`.

### Questão 6 {-}

Faça uma iteração com o loop `for` sobre os valores ímpares e múltiplos de 7 de 500 números aleatórios entre `0` e `1000`.

### Questão 7 {-}

Crie a figura abaixo com o loop `for`.

```{r ex_for_loop, echo=FALSE, fig.align = "center"}
knitr::include_graphics("img/ex_for.png")
```

### Questão 8 {-}

Crie o padrão abaixo com o loop `for`.

```{r ex_for_loop2, echo=FALSE, fig.align = "center"}
knitr::include_graphics("img/ex_for2.png")
```

### Questão 9 {-}

Faça o print dos primeiros 20 números pares (incluindo zero e em ordem inversa) com o loop `for`.

## Respostas

### Questão 1 {-}

```{r, eval=FALSE}
for(numero in 1:1000){
    
    if((numero %% 2) != 0)
        print(numero)
}
```

### Questão 2 {-}

```{r, eval=FALSE}
for(numero in 1:1000){
    
    if(!(numero %% 8) != 0)
        print(numero)
}
```

### Questão 3 {-}

```{r, eval=FALSE}
for(fator in 0:10)
    print(paste("5*", fator, " = ", 5 * fator, sep=""))
```

### Questão 4 {-}

```{r, eval=FALSE}
for(fator_externo in 0:10){
    
    for(fator_interno in 0:10){
        
        print(paste(fator_externo, "*", fator_interno, " = ", fator_externo * fator_interno, sep=""))
    }
    
    if(fator_externo < 10)
        print("----------------------------")
}
```

### Questão 5 {-}

```{r, eval=FALSE}
numeros <- sample(0:1000, 500)

for(numero in numeros){
    
    if(!(numero %% 2))
        print(numero)
}
```

### Questão 6 {-}

```{r, eval=FALSE}
numeros <- sample(0:1000, 500)

for(numero in numeros){
    
    if((numero %% 2 != 0) && !(numero %% 7))
        print(numero)
}
```

### Questão 7 {-}

```{r, eval=FALSE}
for(qtd in 1:10)
    print(rep("*", qtd))
```

### Questão 8 {-}

```{r, eval=FALSE}
produtos <- c("Leite", "Queijo")

animais <- c("Vaca", "Cabra", "Ovelha", "Búfala")

for(produto in produtos){
    
    for(animal in animais)
        print(paste(produto, "de", animal))
}
```

### Questão 9 {-}

```{r, eval=FALSE}
# Primeiros 100 numeros
numeros <- 0:100

# Números pares:
numeros_pares <- numeros[!(numeros %% 2)]

# Primeiros 20:
numeros_pares <- numeros_pares[20:1]

for(numero in numeros_pares)
    print(numero)
```