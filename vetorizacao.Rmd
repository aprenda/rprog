# Vetorização

Chegamos a um capítulo imprescindível tanto em R quanto em programação em geral. Vetorização é essencial na performance de um programa, aumentando a velocidade da execução em centenas de vezes. Neste capítulo, vamos entender sobre o que se trata esta operação e veremos várias funções vetorizadas próprias do R.

## O Que é Vetorização

Antes de definir vetorização tecnicamente, voltemos a nossa função de [partição](#exemplo-vetorizacao), mostrada abaixo.

```{r, comment=""}
particao <- function(vetor){
    #' Partição de um vetor
    #' 
    #' @description particao() é utilizada para separar os números pares e ímpares de um vetor numérico.
    #' 
    #' @param vetor numérico. Um vetor contendo os números para separação.
    #' 
    #' @return particao() retorna uma lista com dois elementos: os números pares e os números ímpares.
    #' 
    #' @examples
    #' vetor <- 0:9
    #'
    #' particao(vetor)
    
    pares <- numeric()
    impares <- numeric()
    
    for(numero in vetor){
        
        if(!(numero %% 2)){
            pares <- c(pares, numero)
        } else{
            impares <- c(impares, numero)
        }
    }
    
    lista_particao <- list(Pares=pares, Ímpares=impares)
    
    return(lista_particao)
}
```

No momento que criamos esta função, notamos que é uma péssima forma de se criar esta função. Vamos enunciar os motivos:

1. Criamos dois vetores numéricos vazios (`pares` e `impares`) que não possuem tamanho total definido, isto é, não alocamos o tamanho correto previamente à execução;
1. Fazemos um loop `for` para iterar sobre todos os itens do objeto;
1. Como não temos um tamanho destes vetores, temos de fazer, dentro do loop `for`, uma concatenação do vetor numérico com um número.

Percebam a quantidade de verificações e execuções que ocorrem a todo momento neste código: fazemos uma iteração sobre todos os elementos e, a cada iteração, verificamos e fazemos uma concatenação nova. Para objetos e operações pequenas, como as nossas, este código não faz tanta diferença na performance final. Contudo, em operações reais, com conjuntos de dados grandes, a performance de nosso programa diminuiria em grande escala. Quanto maior o conjunto de dados e dependendo das operações, maior será a penalidade sobre a performance do programa, fazendo com que a execução seja extremamente mais lenta. É aqui que entra a vetorização — para otimizar o programa. No nosso exemplo, é fácil retirar todas estas execuções desnecessárias e nós já aprendemos como. Veja:

```{r, comment=""}
particao <- function(vetor){
    #' Partição de um vetor
    #' 
    #' @description particao() é utilizada para separar os números pares e ímpares de um vetor numérico.
    #' 
    #' @param vetor numérico. Um vetor contendo os números para separação.
    #' 
    #' @return particao() retorna uma lista com dois elementos: os números pares e os números ímpares.
    #' 
    #' @examples
    #' vetor <- 0:9
    #'
    #' particao(vetor)
    
    # Vetor de pares:
    pares <- vetor[!(vetor %% 2)]
        
    # Vetor de ímpares:
    impares <- vetor[vetor %% 2 != 0]
    
    # Lista:
    lista_particao <- list(Pares=pares, Ímpares=impares)
    
    return(lista_particao)
}
```

Trocamos as iterações e concatenações por uma única operação: uma indexação condicional. Isso mesmo, já vínhamos utilizando vetorização há muito tempo.

Tecnicamente, vetorização é parte de algo muito importante na computação atual: a programação paralela. Em termos simples, vetorização é definida como **uma operação em vários elementos de uma única vez**; uma operação em vetores. Pense no nosso código: antes, estávamos iterando sobre todos os elementos, ou seja, avaliando um por um, e então verificando e concatenando o vetor. Após a modificação, fazemos uma única operação: no vetor inteiro de uma única vez. Isto é a operação de vetorização: menos loops (os quais geralmente são lentos) e mais operações diretamente em vetores. Vetorizações ainda são loops — basta ver que em nossa vetorização, ainda avaliamos todos os elementos do vetor para então selecioná-los — mas normalmente, os desenvolvedores da linguagem criam e otimizam tais loops em linguagens de nível menor, como a linguagem C. Deste modo, os loops vetorizados são muito mais eficientes.

Parece uma excelente ideia, não é? Vetorizar tudo, otimizar o código o máximo possível... Mas isto pode nos levar à otimização prematura — a raiz de todo o mal, segundo [Donald Knuth](https://en.wikipedia.org/wiki/Donald_Knuth). Basicamente, é o ato de perder muito tempo tentando otimizar seu programa sem saber exatamente o que temos de fazer. Para tentar não cair neste problema, o nosso pensamento se baseará em: *"este loop é mesmo necessário?"*; *"este loop pode ser facilmente substituído por algo mais simples?"* Apenas isto: tentaremos evitar loops desnecessários. Se se interessar sobre o assunto, você pode iniciar uma leitura sobre o assunto na seção de [leia mais](#leia-vetorizacao).

Qual a vantagem de R, então? Tudo em R é basicamente um vetor! Lembre-se de que vetores são as pedras fundamentais das estruturas de dados em R — todas as estruturas são vetores ou são, de alguma forma, construídas sobre vetores. Por exemplo, `6` e `"Programação"` são vetores de tamanho `1`. Portanto, vetorização é altamente aplicável em R.

Como agora sabemos, as operações sobre vetores são operações vetorizadas, por definição. Então, as operações abaixo e afins são todas vetorizadas:

```{r, comment=""}
vetor <- 0:10

vetor + 2

vetor / 4

vetor^2

sqrt(vetor)
```

Neste pensamento de usarmos menos loops, veremos nas próximas seções várias funções que, em muitos casos, substituem loops. Em suma, com estas funções (chamadas de **família apply** ou **família \*ply**), temos o poder dos loops, mas com as vantagens de ser um código muito menor, mais legível e, geralmente, mais eficiente. Principalmente com grandes conjuntos de dados. Contudo, antes de entrarmos na família apply em si, conheceremos algo importante que pode ser utilizado nelas: a função anônima.

## Funções Anônimas

Por definição, funções são blocos de código ligados a um nome. Então, o que seriam funções anônimas? Como o próprio nome diz, são funções que não possuem nome, isto é, são funções que não estão ligadas a nenhum nome.

Quando criamos uma ligação, este nome fica ligado a um objeto na memória e, através deste nome, conseguimos acessar tais valores/objetos/funções sem criá-los novamente. Se não criamos um nome e fazemos uma ligação, estas funções não ficam na memória: tais funções são criadas no momento da execução e as perdemos no momento que sua execução termina. Por este motivo, funções anônimas são utilizadas como argumento em outras funções; usadas quando desejamos executar algo apenas uma vez e não precisaremos mais da função. Por isto, são amplamente utilizadas com as funções da família apply, pois as funções apply necessitam de funções como argumentos e, muitas vezes, estas funções são funções anônimas.

Vejamos como criar uma função anônima. Vejamos esta simples função:

```{r, comment=""}
expoente <- function(valor){
    return(valor^2)
}

expoente(5)
```

É uma função normal, como já sabemos fazer. Sua forma anônima seria:

```{r, eval=FALSE}
function(valor){valor^2}
```

Esta é uma função anônima. De fato, a única diferença é a ausência de um nome ligado à função. Este tipo de função não foi feito para ser executado assim, mas, se quiséssemos executá-la, faríamos:

```{r, comment=""}
(function(valor){valor^2})(5)
```

## Função `apply()`

A função `apply()` itera uma função qualquer sobre as margens de um array ou data frame, isto é, executa uma função nas linhas ou colunas de um objeto.

Sintaxe:

```{r, eval=FALSE}
apply(X, MARGIN, FUN, ...)
```

Onde:

- `X`: array ou data frame.
- `MARGIN`: Margem na qual executar a função. Este argumento é numérico: `1` são linhas e `2` são colunas. Basta lembrar como indexamos um objeto 2D: a priemria dimensão (`1`) são as linhas e a segunda dimensão (`2`) são as colunas.
- `FUN`: Função a ser executada na margem.
- `...`: Argumentos passados à função.

Vamos utilizar os dados de `mtcars` para ver esta função na prática. Queremos saber qual a média de cada variável deste conjunto de dados. Na forma de loop, poderíamos fazer:

```{r, comment=""}
medias <- numeric(ncol(mtcars))

names(medias) <- colnames(mtcars)

for(n_col in 1:ncol(mtcars)){
    
    medias[n_col] <- mean(mtcars[, n_col])
}

print(medias)
```

No entanto, podemos simplificar muito este código e eliminar este loop explícito utilizando `apply()`, pois queremos iterar sobre as colunas de um data frame. Veja:

```{r, comment=""}
apply(mtcars, 2, mean)
```

Acima, iteramos a função `mean()` sobre cada coluna (`2`) de `mtcars`. Com isto, não precisamos de um loop explícito para resolver nosso problema.

Perceba que, como é uma função simples — a média de colunas —, poderíamos ter feito:

```{r, eval=FALSE}
colMeans(mtcars)
```

Mas o uso de `apply()` é muito mais amplo do que calcular médias ou somas de linhas e colunas. Com `apply()`, podemos aplicar qualquer função que quisermos às margens do objeto. Utilizando o conjunto de dados `iris`, vamos fazer o cálculo $\frac{\text{sum} \left( coluna \right)}{\text{max}\left( coluna \right)}$ através de uma função anônima.

```{r, comment=""}
apply(iris[, -5], 2, function(x) sum(x)/max(x))
```

> Nota: utilizamos `iris[, -5]` para excluir a coluna de caracteres e evitar erro de cálculo.

Outros exemplos de `apply()`:

```{r, comment=""}
# Valores mínimo e máximo de cada coluna:
apply(mtcars, 2, range)

# Mediana de cada coluna:
apply(mtcars, 2, median)
```

A partir de agora, as outras funções da família *apply* são fáceis de se entender. O pensamento é o mesmo: evitamos loops explícitos ao iterar sobre um objeto com uma função da família apply.

## Função `lapply()`

A função `lapply()` itera e aplica uma função sobre os itens de uma lista ou vetor, retornando uma lista. Podemos pensar como "**L**ist **apply**".

Sintaxe:

```{r, eval=FALSE}
lapply(X, FUN, ...)
```

Onde:

- `X`: lista.
- `FUN`: Função a ser executada em cada elemento da lista.
- `...`: Argumentos passados à função.

Como primeiro exemplo, vamos iterar sobre uma lista simples. Criando a lista:

```{r, comment=""}
lst <- list(
    sample(0:100, 10),
    sample(0:200, 10),
    sample(0:300, 10)
)
```

Agora vamos calcular a média e intervalo de cada elemento desta lista, isto é, de cada vetor numérico.

```{r, eval=FALSE}
# Média:
lapply(lst, mean)

# Intervalo:
lapply(lst, range)
```

Agora um exemplo mais prático: vamos voltar à [questão 12](#q12) do capítulo de vetores atômicos. Nesta questão, temos muitos vetores e queremos escolher somente os vetores que possuem somente elementos maiores do que 10. Para verificar esta condição, naquele capítulo, fizemos avaliações individuais do tipo `all(vetor > 10)`, resultando em muitas avaliações. Agora, podemos fazer isto de uma forma muito mais simples com `lapply()`. Nosso conjunto de dados é:

```{r, comment=""}
v1 <- c(10.4, 10.7, 15.1, 13.1, 17.2, 18.7, 18.9, 18.4, 16.5, 13.8)

v2 <- c(14.4, 17.3, 12.7, 13.1, 12.6, 19.9, 15.5, 16.5, 9.8, 19.3)

v3 <- c(15.5, 13.4, 16.7, 10.1, 13.0, 12.4, 17.1, 13.9, 10.8, 12.9)

v4 <- c(8.0, 16.4, 17.5, 18.0, 9.7, 10.0, 17.4, 17.6, 12.3, 10.1)

v5 <- c(9.2, 17.8, 12.1, 13.7, 16.2, 10.9, 9.3, 13.8, 14.7, 9.4)

v6 <- c(18.4, 8.2, 9.5, 12.0, 11.5, 12.5, 16.5, 13.4, 15.8, 8.4)

v7 <- c(8.0, 14.9, 8.2, 19.9, 16.4, 16.5, 19.5, 12.0, 17.2, 15.7)

v8 <- c(16.4, 15.7, 16.2, 17.2, 9.3, 11.1, 9.1, 8.9, 10.9, 13.5)

v9 <- c(17.1, 13.4, 15.9, 11.2, 18.4, 15.3, 14.9, 16.1, 11.0, 14.5)

v10 <- c(12.5, 9.6, 12.7, 9.4, 10.7, 15.2, 13.2, 10.3, 18.8, 17.7)
```

Primeiramente, precisamos ter uma lista, ou seja, colocar cada vetor deste como elemento de uma única lista:

```{r, comment=""}
lst_vetores <- list(
    v1, v2, v3, v4, v5,
    v6, v7, v8, v9, v10
)
```

E vamos verificar com `lapply()`:

```{r, comment=""}
lapply(lst_vetores, function(x) all(x > 10))
```

Utilizamos uma função anônima que avalia cada elemento da lista (cada vetor) e avalia se todos elementos são maiores que 10. Como `lapply()` retorna uma lista, os elementos da lista retornada respondem se o vetor em questão possui somente valores maiores que 10.

Um detalhe sobre `lapply()` é que se o objeto não for uma lista, ele será coagido a tal. Vamos utilizar `lapply()` com um vetor:

```{r, comment=""}
vetor <- 0:10

lapply(vetor, sample)
```

Acima, `lapply()` tranformou `vetor` em uma lista, e então, aplicou `sample` sobre cada elemento da lista. É o mesmo que:

```{r, comment=""}
lst <- list(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10)

lapply(lst, sample)
```

E podemos passar argumentos à função sendo executada:

```{r, comment=""}
lapply(vetor, sample, size=20, replace=TRUE)
```

Acima, passamos os argumentos `size=20` e `replace=TRUE` para a função `sample()` sendo executada.

## Função `sapply()`

A função `sapply()` é uma forma simplificada de `lapply()`. Podemos pensar como "**S**implified l**apply**". Sendo uma versão de `lapply()`, esta função também itera sobre listas e vetores. Sua diferença é que `sapply()` tenta retornar um objeto mais simples do que lista, quando possível, uma vez que `lapply()` sempre retorna listas. O resultado de `sapply()` será:

- Um vetor, se os elementos da lista resultante forem todos de tamanho `1`;
- Uma matriz, se os elementos da lista resultante forem todos de mesmo tamanho, além deste tamanho ser maior do que `1`;
- Uma lista, se as outras condições acima não forem satisfeitas.

Vamos voltar ao nosso exemplo dos vetores, visto em `lapply()`.

```{r, comment=""}
sapply(lst_vetores, function(x) all(x > 10))
```

Na seção passada, nosso resultado foi uma lista com todos estes elementos. Com `sapply()`, o objeto retornado foi simplificado para um vetor com todos os valores booleanos, pois cada elemento da lista (cada valor booleano) possui tamanho `1`.

Vamos criar uma lista de matrizes:

```{r, comment=""}
mat <- list(
    matrix(1:4, 2, 2),
    matrix(5:8, 2, 2),
    matrix(9:12, 2, 2)
)
```

Agora, vamos extrair a primeira coluna de cada uma destas matrizes:

```{r, comment=""}
# Using an anonymous function. sapply gives three vectors of equal length 2, result is a matrix
sapply(mat, function(matriz) matriz[, 1])
```

Acima, nos é retornado uma matriz na qual cada coluna é a primeira coluna de uma das matrizes. Isto porque cada resultado da lista (cada coluna) tem dois elementos (tamanho `2`).

## Função `tapply()`

A função `tapply()` executa uma função nos subconjuntos de um array, isto é, em grupos (fatores) de um array. Sua sintaxe é:

Sintaxe:

```{r, eval=FALSE}
tapply(X, INDEX, FUN, ...)
```

Onde:

- `X`: Objeto. Tecnicamente, um objeto no qual podemos executar `split()`.
- `INDEX`: Fatores ou lista de fatores para aplicarmos a função; os grupos.
- `FUN`: Função a ser executada em cada grupo.
- `...`: Argumentos passados à função.

Como exemplo, vamos aplicar `tapply()` ao conjunto de dados `iris`. Vamos calcular a média do tamanho das sépalas de cada espécie:

```{r, comment=""}
tapply(X=iris$Sepal.Length, INDEX=iris$Species, FUN=mean)
```

Podemos ler desta forma:

- Média: `FUN=mean`
- Tamanho das sépalas: `X=iris$Sepal.Length`; são os objetos, os dados a serem calculados.
- Cada espécie: `INDEX=iris$Species`; os grupos (fatores) são as espécies.

## Leia Mais {#leia-vetorizacao}

### Otimização Prematura {-}

- [Wiki](http://wiki.c2.com/?PrematureOptimization)
- [Effectiviology](https://effectiviology.com/premature-optimization/)
- [Stackify](https://stackify.com/premature-optimization-evil/)
- [Stack Exchange](https://softwareengineering.stackexchange.com/questions/80084/is-premature-optimization-really-the-root-of-all-evil)

### Vetorização {-}

- [Ask Me Code](https://www.askmecode.com/blog/why-vectorization-is-important-in-computers/)
- [Intel](https://www.intel.com/content/www/us/en/developer/articles/technical/vectorization-a-key-tool-to-improve-performance-on-modern-cpus.html)