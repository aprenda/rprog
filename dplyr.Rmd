# `dplyr`

Já estamos um pouco familizarizados com o pacote `dplyr`! Quando usamos *piping* e NSE, utilizamos `dplyr`. O operador de *piping* (`%>%`) é de um pacote chamado `magrittr`, o qual `dplyr` importa automaticamente para usar. O operador `.` (*dot notation*) e seu significado de "conter o resultado da expressão anterior do pipe", também é do pacote `magrittr`, sendo a unidade básica de transferência de valores em `magrittr`. Quando, em um capítulo passado, carregamos `library(tidyverse)`, o pacote `dplyr` (e, consequentemente, `magrittr`) foi carregado e por isso conseguimos utilizar *piping* e as notações como o operador `.`. Adicionalmente, a função `glimpse()` que vimos anteriormente também é do pacote `dplyr()`.

Com o pacote `dplyr`, podemos alterar de diferentes formas um conjunto de dados. Alguns dos principais usos são: adicionar/remover variáveis; selecionar linhas/colunas específicas (e acessar qualquer outro tipo de subconjunto); ter resumos dos dados (média/soma de valores ou qualquer outro cálculo necessário); e agrupar variáveis de interesse e tabalhar somente com estas.

Podemos instalar e carregar `dplyr` individualmente com:

```{r, eval=FALSE}
install.packages("dplyr")

library(dplyr)
```

`dplyr` possui diversos "verbos" (funções). As principais são:

- `mutate()`: Cria variáveis (colunas) novas ou modifica existentes.
- `select()`: Seleciona variáveis específicas.
- `filter()`: Seleciona linhas de acordo com alguma condição.
- `summarize()`: Executa cálculos em grupos de variáveis.
- `group_by()`: Agrupa variáveis.
- `arrange()`: Ordena linhas de acordo com o valor de uma coluna.

Vamos importar um conjunto de dados para trabalharmos cada uma destas funções. Utilizaremos nosso conjunto `filmes_terror.csv`.

```{r, echo=FALSE, results='hide'}
filmes <- read_csv("datasets/filmes_terror.csv")
```

```{r, eval=FALSE}
filmes <- read_csv("filmes_terror.csv")
```

```{r, comment=""}
glimpse(filmes)
```

Preste atenção nas seções seguintes, pois vamos trabalhar **sequencialmente** com este conjunto de dados!

## `filter()`

Com `filter()`, podemos selecionar as linhas desejadas; acessar um subconjunto de linhas de acordo com alguma(s) condição(ões). Como primeiro argumento de `filter()`, temos o tibble que desejamos trabalhar, enquanto os argumentos seguintes são as condições para a filtragem de linhas.

Vamos selecionar todos os filmes a partir do ano de 2017:

```{r, comment=""}
filmes %>% 
    filter(., Ano >= 2017)
```

Com a função `count()`, podemos contar o número de observações (linhas):

```{r, comment=""}
filmes %>% 
    filter(., Ano >= 2017) %>% 
    count()
```

Temos 12,320 filmes que foram lançados a partir de 2017. E os filmes lançados a partir de 2017 com status `Concluído`?

```{r, comment=""}
filmes %>% 
    filter(., Ano >= 2017, Status == "Concluído") %>% 
    count()
```

Ao total, 11,933 filmes foram concluídos desde 2017. 

Perceba que: dividimos as condições com vírgulas, isto é, cada condição é um argumento da função `select()`. Esta sintaxe é mais simples e legível. Ainda assim, poderíamos fazer uma condicional de "R puro" (como abaixo), mas não é recomendada.

```{r, comment=""}
filmes %>% 
    filter(., Ano >= 2017 & Status == "Concluído") %>% 
    count()
```

Um último detalhe: nunca se esqueça de que sempre trabalhamos em uma sequência de comandos! Veja os comandos acima e perceba as sequências feitas.

## `mutate()`

Com `mutate()`, podemos: criar novas variáveis; criar variáveis novas baseadas em variáveis existentes; modificar variáveis existentes; e qualquer outra "mutação" (alteração) do conjunto de variáveis em questão.

Olhando o conjunto de dados, podemos transformar várias destas variáveis em fatores. Como a maioria é um vetor de caracteres, podemos fazer este processo de uma única vez com `mutate_if()` — uma variação de `mutate()`.

A função `mutate_if()` altera variáveis de acordo com iuma condição (por isto o `if`). Neste caso, nossa condição é a variável ser do tipo "caracteres". Se for, ela será mudada para um fator. Se não for, continuará a mesma.

```{r, comment=""}
filmes %>% 
    mutate_if(is.character, factor) %>% 
    glimpse()
```

Acima, fizemos toda a mudança na linha `mutate_if(is.character, factor)`. Vamos destrinchar:

- `mutate_if`: função para alterar as variáveis.
- `is.character`: condição. Só serão alteradas as variáveis que forem caracteres.
- `factor`: função que queremos aplicar nas variáveis selecionadas. Neste caso, transformaremos todas em fatores.

> Nota: Muitos dos verbos (funções) em dplyr possuem variações como `*_if`, `*_at`, etc. Se você souber a função principal, as variações são consequência.

Perceba que não criamos uma variável para guardar estes resultados, então não salvamos as mudanças. Vamos salvar:

```{r, comment=""}
filmes <- filmes %>% 
    mutate_if(is.character, factor)
```

Mesmo sendo um bom exercício, não faz sentido coagir todas variáveis do nosso conjunto de dados para fator. Algumas delas fazem melhor sentido como caracteres, como `Título`, `Duração`, `Sinopse`, `Palavras-chave` e `Data de Lançamento`. Vamos mudar novamente estas para caracteres e salvar o resultado:

```{r, comment=""}
filmes <- filmes %>% 
    mutate_if(is.character, factor) %>% 
    mutate(
        across(c(Título, Duração, Sinopse, `Palavras-chave`, `Data de Lançamento`), as.character)
    )

glimpse(filmes)
```

O comando que utilizamos foi:

```{r, eval=FALSE}
mutate(across(c(Título, Duração, Sinopse, `Palavras-chave`, `Data de Lançamento`), as.character))
```

Destrinchando:

- `mutate()`: função para modificar as variáveis.
- `across()`: função do pacote `dplyr`. Usamos `across()` para executar uma função em várias colunas específicas. Assim, fornecemos à ela todas as variáveis que queremos modificar, assim como a função para modificá-las.
- `c(Título, Duração, Sinopse, Palavras-chave, Data de Lançamento)`: Variáveis que queremos modificar.
- `as.character`: função para coagir para caracteres.

Vamos exemplificar agora a criação de uma nova variável. Suponha que deseja ver quantos filmes desta lista você já assistiu. Vamos criar uma variável de fatores chamada `Assistido` na qual todos os valores iniciais são `FALSE`:

```{r, comment=""}
filmes <- filmes %>% 
    mutate_if(is.character, factor) %>% 
    mutate(
        across(c(Título, Duração, Sinopse, `Palavras-chave`, `Data de Lançamento`), as.character)
    ) %>% 
    mutate(Assistido = FALSE)

glimpse(filmes)
```

Para criar esta nova variável, fizemos uma atribuição na forma `nome = valor` dentro de `mutate()`. O comando foi: `mutate(Assistido = FALSE)`, sendo o nome (da coluna) `Assistido` e o valor de todas as linhas é `FALSE`. Perceba que `dplyr` colocou a nova variável como última coluna do tibble, pois este é o default sempre. Agora, você pode passar sobre a lista de filmes e alterar para `TRUE` todos aqueles que já assistiu.

Como último exemplo, vamos criar uma nova variável baseada em outra. Vamos criar uma variável chamada `Nota_por_qtd` que é a razão entre a nota dos usuários e a quantidade total de votos.

```{r, comment=""}
filmes <- filmes %>% 
    mutate_if(is.character, factor) %>% 
    mutate(
        across(c(Título, Duração, Sinopse, `Palavras-chave`, `Data de Lançamento`), as.character)
    ) %>% 
    mutate(Assistido = FALSE) %>% 
    mutate(Nota_por_qtd = `Notas dos Usuários` / `Quantidade Total de Votos dos Usuários`)

glimpse(filmes)
```

Veja que bastou colocarmos o nome da nova variável (`Nota_por_qtd`) e atribuirmos à ela a razão desejada:

$\text{Nota_por_qtd}=\frac{\text{Notas dos Usuários}}{\text{Quantidade Total de Votos dos Usuários}}$

## `select()`

A função `select()` nos permite escolher variáveis (colunas) espeícifas do tibble e diminuir o tamanho de colunas do conjunto de dados.

Como primeiro exemplo, vamos selecionar apenas os anos de lançamento dos filmes:

```{r, comment=""}
filmes %>% 
    select(Ano)
```

Toda vez que trabalhamos em um tibble e executamos qualquer função, ela é executada em **todas** as colunas. Como selecionamos apenas esta coluna, qualquer execução que façamos a partir da seleção trabalhará somente com ela. Vamos ver o intervalo de anos destes filmes:

```{r, comment=""}
filmes %>% 
    select(Ano) %>%
    range(.)
```

Nosso retorno foi `NA`. Claramente, em algum lugar desta variável existe um ou mais valores `NA`. Vamos remover estas linhas com `filter()`:

```{r, comment=""}
filmes %>% 
    select(Ano) %>%
    filter(!is.na(.)) %>% 
    range(.)
```

Agora, sem `NA`'s, nosso conjunto de dados abarca filmes desde o ano 1896. Qual filme é este?

```{r, comment=""}
filmes %>% 
    filter(Ano == 1896) %>% 
    select(Título)
```

Estes quatro filmes acima foram lançados no ano de 1896, segundo o conjunto de dados.

E se quisermos acessar mais de uma coluna? Basta indicarmos quais outras colunas queremos.

```{r, comment=""}
filmes %>% 
    filter(Ano == 1896) %>% 
    select(Título, Nota_por_qtd, Gênero)
```

Podemos também remover colunas:

```{r, comment=""}
# Removendo uma coluna:
filmes %>% 
    select(-Título)
```

```{r, comment=""}
# Removendo várias colunas:
filmes %>% 
    select(-c(Título, Status, Sinopse))
```

Ou selecionar de acordo com uma condição. Vamos selecionar todas as colunas que numéricas com a variação `select_if()`:

```{r, comment=""}
filmes %>% 
    select_if(is.numeric)
```

## `arrange()`

Com `arrange()`, temos uma ordenação das linhas de acordo com uma variável. Seu uso é simples: fornecemos o tibble com os dados e a(s) coluna(s) que queremos ordenar. Vamos ordenar `filmes` de acordo com as notas dos usuários:

```{r, comment=""}
filmes %>% 
    arrange(., `Notas dos Usuários`) %>% 
    select(Título, Ano, `Notas dos Usuários`)
```

Por default, `arrange()` ordena de forma crescente. Usamos `desc()` para ordenar de forma decrescente de acordo com alguma coluna:

```{r, comment=""}
# Ordenando por 'Notas dos Usuários' decrescente:
filmes %>% 
    arrange(., desc(`Notas dos Usuários`)) %>% 
    select(Título, Ano, `Notas dos Usuários`)
```

Vamos ordenar de acordo com nossa variável `Nota_por_qtd`:

```{r, comment=""}
# Ordenando por 'Nota_por_qtd' decrescente:
filmes %>% 
    arrange(., desc(Nota_por_qtd)) %>% 
    select(Título, Ano, `Notas dos Usuários`, `Quantidade Total de Votos dos Usuários`, Nota_por_qtd)
```

Mas estamos ordenando uma razão: quanto maior o denominador (neste caso, o total de pessoas votando), menor será o resultado da divisão. Assim, queremos os menores números, que significam que muitas pessoas votaram:

```{r, comment=""}
# Ordenando por 'Nota_por_qtd' crescente:
filmes %>% 
    arrange(., Nota_por_qtd) %>% 
    select(Título, Ano, `Notas dos Usuários`, `Quantidade Total de Votos dos Usuários`, Nota_por_qtd)
```

Ou simplesmente podemos ordenar por quantidade total de pessoas:

```{r, comment=""}
# Ordenando por 'Quantidade Total de Votos dos Usuários' decrescente:
filmes %>% 
    arrange(., desc(`Quantidade Total de Votos dos Usuários`)) %>% 
    select(Título, Ano, `Notas dos Usuários`, `Quantidade Total de Votos dos Usuários`, Nota_por_qtd)
```

Mas também podemos ordenar de acordo várias colunas. Vamos ordenar de acordo com: `Quantidade Total de Votos dos Usuários`, `Notas dos Usuários` e `Nota_por_qtd` (nesta ordem).

```{r, comment=""}
filmes %>% 
    arrange(., desc(`Quantidade Total de Votos dos Usuários`), `Notas dos Usuários`, Nota_por_qtd) %>% 
    select(Título, Ano, `Quantidade Total de Votos dos Usuários`, `Notas dos Usuários`, Nota_por_qtd)
```

Quando especificamos mais de uma coluna, cada coluna adicional será utilizada para desempatar os valores das colunas anteriores.

## `summarize()`

Como já dito, por default, trabalhamos com o tibble inteiro em qualquer execução que fizermos com os dados. Com `group_by()`, agrupamos os dados desejados e, então, todas as execuções serão pertinentes somente às variáveis agrupadas. Vamos agrupar nossos dados por ano e status:

```{r, comment=""}
filmes %>% 
    group_by(Ano, Status)
```

Perceba o início do print:

```
# A tibble: 43,772 × 18
# Groups:   Ano, Status [138]
```

`dplyr` está nos falando que há agrupamentos no conjunto de dados, a saber, `Ano` e `País` (nesta ordem). Quaisquer execuções que fizermos agora, serão relacionadas a estas variáveis. Contudo, este é somente um agrupamento; dissemos ao `dplyr` que estamos interessados nestas variáveis. Agora, temos usá-las. A função `group_by()` é muito útil com a função `summarize()`, a qual nos permite "resumir" tais grupos de acordo com algum cálculo. Por exemplo, vamos calcular a média de notas por ano e país:

```{r, comment=""}
filmes %>% 
    group_by(Ano, País) %>% 
    summarise(., media = mean(`Notas dos Usuários`))
```

Criamos uma nova variável (o "resumo" do agrupamento) chamada `media` que é a média das notas. O comando foi: `media = mean(Notas dos Usuários)`.

Veja que o agrupamento é explícito: para cada ano, cada páis tem sua média individual. A hierarquisa do agrupamento para os três primeiros anos está descrito na tabela abaixo (compare com o tibble):

- 1896:
    - France: 6.325
- 1897:
    - France: 5.78
    - UK: 5.9
    - USA: 6
- 1898:
    - France: 6.520000
    - Japan: NA
    - UK: 5.2
    - USA: 5.4

Mas temos valores ausentes no conjunto de dados, podemos retirar com o argumento `na.rm = TRUE` de `mean()`:

```{r, comment=""}
filmes %>% 
    group_by(Ano, País) %>% 
    summarise(., media = mean(`Notas dos Usuários`, na.rm = TRUE))
```

E podemos ordenar de acordo com as maiores médias:

```{r, comment=""}
filmes %>% 
    group_by(Ano, País) %>% 
    summarise(., media = mean(`Notas dos Usuários`, na.rm = TRUE)) %>% 
    arrange(., desc(media))
```

