# ggplot2

Na parte anterior deste livro, vimos como fazer gráficos com R puro. Agora vamos aprender como fazer gráficos com `ggplot2`, um pacote do tidyverse que é indispensável ao se fazer gráficos estáticos em R. Vamos começar pelo nome: **gg**plot2 deriva de "**G**rammar of **G**raphics" — a gramática dos gráficos. O que é esta gramática?

[Gramática dos Gráficos](https://link.springer.com/book/10.1007/0-387-28695-0) é um livro publicado em 1999 (segunda edição em 2005) pelo estatístico estadunidense Leland Wilkinson. Esta "gramática" de Leland demonstra como produzir gráficos quantitativos em quaisquer de suas formas e em alta qualidade. Para isto, ele destrincha um gráfico estatístico e identifica suas partes fundamentais, as quais fundamentam a gramática.

A gramática dos gráficos baseia-se no pensamento de que os gráficos quantitativos possuem o mesmo conjunto de elementos (as partes fundamentais): o conjunto de dados, um sistema de coordenadas, os atributos estéticos e objetos geométricos. O conjunto de dados é auto-explicativo — são os dados que queremos visualizar. O sistema de coordenadas define os locais dos pontos (os dados) no gráfico e também pode conter transformações dos dados. Os atributos estéticos são a representação visual dos dados; como os dados são mostrados no gráfico: cor, tamanho, borda. Objetos geométricos significam como os dados serão mostrados no gráfico; a forma do gráfico: um gráfico de linhas, de pontos, de barras, um histograma, um boxplot, etc. A junção de todos estes elementos é o que forma um gráfico quantitativo.

O pacote `ggplot2` é a implementação da gramática dos gráficos na linguagem R. Podemos então fazer uma extrema simplificação e dizer que `ggplot2` baseia-se na relação:

$\text{dados} \sim \text{estética} + \text{geometria}$

Isto é, os dados são mapeados (ligados) a alguma estética e geometria (os três componentes principais do ggplot). Exemplo: os dados mapeados à geometria de pontos e estética da variáveis nos eixos X e Y em tamanho 4 e cor azul formam um gráfico de pontos onde os pontos de ambas variáveis nos eixos X e Y são da cor azul e possuem tamanho 4.

Por causa de todos estes mapeamentos (relações dos dados e os outros componentes), é facil pensarmos no gráfico em ggplot como uma construção em camadas. Este é um dos motivos pelo sucesso do ggplot: todas estas camadas — todos estes componentes — podem ser criadas, removidas e/ou modificadas de acordo com a necessidade em questão, criando uma possibilidade gigantesca de personalização de gráficos. Então, a questão não é: "é  possível como fazer isto?", mas sim, "qual componente deve ser manipulado (e como)?"

Vamos resumir os principais elementos de um gráfico em ggplot:

- Conjunto de dados.
- Estética: mapeamento das variáveis. Adicionalmente também podemos outros componentes aos dados, como cores e preenchimentos. Em ggplot, definimos os mapeamentos com a função `aes()`, de *aesthetic*.
- Geometria: forma da representação dos dados — linhas, pontos, etc. Em ggplot, os objetos geométricos são definidos como `geom`.
- Sistema de coordenadas: o mapeamento dos dados ao plano 2D do gráfico; define eixos e projeção dos dados. Em ggplot, o sistema de coordenadas é definido como `coord`.

Além destes, um gráfico em `ggplot2` possui outros componentes como `facet` e `theme`, os quais veremos adiante.

> Nota: uma **camada** em ggplot é tecnicamente definida como um conjunto de objetos geométricos e transformações estatísticas (transformações nem sempre são necessárias, mas podem ser feitas). Em geral, criamos camadas cada vez que criamos `geom` diferentes.

Veremos todos estes componentes em ação em breve. Primeiro, vamos instalar e carregar individualmente este pacote:

```{r, eval=FALSE}
install.packages("ggplot2")

library(ggplot2)
```

Por fim, vamos carregar também alguns conjuntos de dados inclusos no `ggplot2`:.

```{r, comment=""}
data(diamonds)

data(mpg)

data(iris)
```

Utilizaremos estes dados ao longo deste capítulo.

## Gráfico de Pontos

### Comparação

Para começarmos, é interessante comparar um gráfico em R puro (pacote base do R) e em ggplot. Vamos criar um scatterplot básico, sem modificação, e ver as diferenças entre os dois pacotes. Vamos criar o conjunto de dados:

```{r, comment=""}
vetor_x <- runif(1000, min = -5, max = 5)

vetor_y <- vetor_x + rnorm(1000)
```

E seu gráfico no pacote base do R:

```{r, comment=""}
plot(
    x=vetor_x,
    y=vetor_y
)
```

Deixemos este gráfico ao lado, por pouco tempo. Agora, precisamos fazer nosso primeiro gráfico em `ggplot2`. Façamos isto em partes.

A função principal do gráfico é `ggplot()`, para a qual fornecemos o conjunto de dados e o mapeamento básico. Aqui vemos nossa primeira diferença: perceba no gráfico em R puro, que fornecemos ao `plot()` as duas variáveis diretamente, isto é, `plot()` possui argumentos próprios para os eixos X e Y e fornecemos os dados diretamente a este argumentos com: `x=vetor_x, y=vetor_y` (veja acima). A função `ggplot()` não aceita variáveis diretamente, mas sim, um conjunto de dados. Em `ggplot()`, fornecemos o conjunto de dados e **depois** mapeamos as variáveis desejadas deste conjunto de dados, ou seja, especificamos as variáveis que serão utilizadas. Portanto, precisamos mudar nossos dados em um data frame (ou tibble), pois é a estrutura de dados ideal para guardarmos os dados. Como já conhecemos tibbles, trabalharemos com eles.

```{r, comment=""}
dados_tbl <- tibble(
    dados_x = vetor_x,
    dados_y = vetor_y,
)

glimpse(dados_tbl)
```

Agora, o input de `ggplot()` será o conjunto de dados `dados_tbl`.

```{r, comment=""}
ggplot(dados_tbl)
```

Temos um plano vazio, sem dados ou estética. Falta especificarmos o mapeamento básico: quais variáveis utilizaremos? Como elas serão mapeadas ao sistema de coordenadas? Utilizaremos as variáveis `dados_x` e `dados_y`, as quais serão mapeadas, respectivamente, aos eixos X e Y. Faremos este mapeamento com a função `aes()` e seus argumentos `x` e `y` (os eixos):

```{r, comment=""}
ggplot(dados_tbl, aes(x=dados_x, y=dados_y))
```

Agora, temos o mapeamento básico do gráfico: um sistema de coordenadas no qual o eixo X está relacionado aos dados de `dados_x` e o eixo Y está relacionado aos dados de `dados_y`. Falta especificarmos outro componente estético básico: a geometria; temos de dizer ao ggplot a forma na qual queremos visualizar o gráfico. Como os dados destas variáveis serão mostradas no plano?

Queremos um *scatterplot*, ou seja, um gráfico de pontos. Assim, nossa geometria, nossos objetos geométricos, são pontos. Em ggplot, todas as geometrias são especificadas em funções com o prefixo `geom_*`. Como queremos pontos, então nossa função será `geom_point()`.

```{r, comment=""}
ggplot(dados_tbl, aes(x=dados_x, y=dados_y)) +
    geom_point()
```

Pronto! Temos nosso primeiro gráfico básico em ggplot.

Importante: veja que temos a função principal `ggplot()` e **adicionamos** uma camada com a função `geom_point()`, ou seja, os pontos são uma camada do gráfico. Por isto, fizemos uso do operador `+` para adicionar esta camada. Isto é um padrão: sempre que criarmos o gráfico com a função `ggplot()`, somente adicionaremos os outros componentes a ele. Adicionalmente, colocamos uma indentação para explicitar a hierarquia do gráfico e seus componentes.

Façamos nossa comparação de um plot básico e sem modificação:

```{r, echo=FALSE}
plot(
    x=vetor_x,
    y=vetor_y,
    main="Pacote Base do R"
)

ggplot(dados_tbl, aes(x=dados_x, y=dados_y)) +
    geom_point() +
    labs(title="Ggplot2")
```

|R puro|Ggplot2|
|:-|:-|
|Background branco e sem grid|Background cinza e com grid|
|Pontos maiores|Pontos menores|
|Pontos sem preenchimento e com borda preta|Pontos com preenchimento preto, assim como as bordas|
|Contém as quatro linhas dos eixos (o quadrado do gráfico)|Não contém as linhas dos eixos; o limite do gráfico é visto pelo background e o grid|
|Marcações dos eixos com escala menor e números inteiros|Marcações dos eixos abarcando toda a amostragem e com números decimais|

É claramente perceptível que o pacote base do R, por default, é extremamente mais simples que o default do ggplot. Por default, o gráfico do ggplot é mais agradável e contém mais elementos.

Os gráficos do pacote base do R foram feitos pelos criadores do R com base na linguagem S (como todo o resto da linguagem) e possuem uma filosofia totalemnte diferente: não há componentes individuais do gráfico — para mudarmos algo, criamos todo o gráfico novamente. Podemos, de fato, adicionar elementos ao gráfico, como linhas e eixos adicionais (funções independentes `lines()` e `axes()`), mas o gráfico pronto já é base destes desenhos adicionais. Em ggplot, temos um sistema muito mais atual e modularizado de se criar gráficos.

### `diamonds`

Vamos fazer outro exemplo, agora com `diamonds`. Vejamos a relação entre `price` e `carat`:

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat)) +
    geom_point()
```

### Forma Alternativa

Para produzir o gráfico, fornecemos os dados diretamente na função:

```{r, eval=FALSE}
ggplot(dados_tbl, aes(x=dados_x, y=dados_y)) +
    geom_point()
```

Mas também podemos utilizar *piping*:

```{r, comment=""}
dados_tbl %>% 
    ggplot(., aes(x=dados_x, y=dados_y)) +
        geom_point()
```

Assim, podemos manipular o conjunto de dados e, quando estiver pronto, adicionar o gráfico à sequência.

## Modificação Básica

Podemos modificar um gráfico em ggplot de diversas formas. Em geral, todas as formas de modificação (como cor, borda, tamanho, etc.) aplicam-se da mesma forma às diversas geometrias (tipos de gráficos). Portanto, a não ser que explicitado, as formas aqui demonstradas funcionam da mesma forma para os outros tipo de gráficos.

### Símbolos

Para um gráfico de pontos, podemos especificar diversos símbolos diferentes para representar os pontos. Estes símbolos são os mesmos que vimos nos gráficos em R puro — os símbolos de 1 a 25.

Para trocar o símbolo, basta indicarmos, na geometria, o número do símbolo desejado no argumento `shape`:

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat)) +
    geom_point(shape=8)
```

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat)) +
    geom_point(shape=11)
```

### Cores e Preenchimento

Já conhecemos o funcionamento mais básico de cores em ggplot: podemos especificar uma cor (ou mais) com string ou um número hexadecimal. Fazemos isto especificando, na geometria, o argumento `color`. Exemplo:

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat)) +
    geom_point(color="skyblue")
```

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat)) +
    geom_point(color="#0A2DFF")
```

Por default, os pontos não possuem borda, somente cor. Então precisamos escolher um símbolo diferente. Vamos trabalhar com um ponto que possua borda e preenchimento: `shape = 21`.

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat)) +
    geom_point(shape=21)
```

Por default, os pontos são ocos (sem preenchimento) e com borda preta. Precisamos definir o preenchimento desta geometria com o argumento `fill`, enquanto o argumento `color` especifica a cor da borda:

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat)) +
    geom_point(shape=21, fill="skyblue", color="white")
```

Fizemos este último gráfico por fins de exercício, pois é claro que a borda, neste caso, mais atrapalha do que ajuda a visualização dos dados.

### Tamanho

Definimos o tamanho dos pontos com o argumento `size`:

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat)) +
    geom_point(color="skyblue", size=4)
```

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat)) +
    geom_point(color="skyblue", size=2.5)
```

### Mapeando Modificações

Nos casos, acima, definimos valores fixos para a geometria (dentro de `geom_point()`). Podemos mapear estes mesmos elementos a alguma variável e ggplot produzirá um gráfico de acordo com esta variável. Exemplo:

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat, color=cut)) +
    geom_point()
```

Primeiro detalhe: veja que, para mapearmos um elemento, precisamos defini-lo na estética do gráfico (dentro de `aes()`). Antes, definimos uma cor específica, fixa, por isto não era mapeada a nenhuma variável e não a colocamos dentro de `aes()`. Neste caso, definimos que a cor dos pontos devem ser relacionadas ao corte do diamante, por isto o código:

```{r, eval=FALSE}
ggplot(diamonds, aes(x=price, y=carat, color=cut))
```

E definimos `color = cut`, onde `cut` é a variável.

Ggplot automaticamente reconheceu que há diferentes classes de cortes e, consequentemente, diferentes cores de pontos, as quais aparecem na legenda gerada automaticamente.

Podemos mapear outros elementos às variáveis disponíveis, de acordo com o que julgar melhor para seu gráfico. Outro exemplo: vamos mapear a forma dos pontos ao corte dos diamantes:

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat, shape=cut)) +
    geom_point()
```

Agora temos um problema: todos os pontos são pretos. Podemos reslver isto mapeando as cores também aos cortes.

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat, shape=cut, color=cut)) +
    geom_point()
```

Como temos várias classes de cores, podemos mudar as cores com uma paleta pré-definida utilizando funções como `scale_color_brewer()` e o argumento `palette`:

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat, shape=cut, color=cut)) +
    geom_point() +
    scale_color_brewer(palette = "Set2")
```

Todas as paletas desta função pode ser vista com:

```{r, comment=""}
RColorBrewer::display.brewer.all()
```

Ademais, podemos definir manualmente as cores com `scale_color_manual()` e o argumento `values`:

```{r, comment=""}
ggplot(diamonds, aes(x=price, y=carat, shape=cut, color=cut)) +
    geom_point() +
    scale_color_manual(
        values=c("black", "pink", "yellow", "brown", "red")
    )
```

## Gráfico de Linhas

Como sabemos agora, definimos cada geometria com uma função da família `geom_*`. Para criarmos um gráfico de linhas, a função é `geom_line()`. Vamos usar o conjunto de dados `economics` e visualizar o desemprego ao longo de algumas décadas:

```{r, comment=""}
ggplot(economics, aes(x=date, y=unemploy)) +
    geom_line()
```

### Modificações

Podemos modificar a cor da linha com `color`:

```{r, comment=""}
ggplot(economics, aes(x=date, y=unemploy)) +
    geom_line(color="forestgreen")
```

Podemos mudar o tipo de linha com `linetype`:

```{r, comment=""}
ggplot(economics, aes(x=date, y=unemploy)) +
    geom_line(color="forestgreen", linetype=2)
```

```{r, comment=""}
ggplot(economics, aes(x=date, y=unemploy)) +
    geom_line(color="forestgreen", linetype=6)
```

E a espessura da linha com `linewidth`:

```{r, comment=""}
ggplot(economics, aes(x=date, y=unemploy)) +
    geom_line(color="forestgreen", linetype=6, linewidth=4)
```

```{r, comment=""}
ggplot(economics, aes(x=date, y=unemploy)) +
    geom_line(color="forestgreen", linetype=6, linewidth=1.2)
```

### Adicionando Linhas

E se quisermos um gráfico com mais linhas? Basta adicionarmos outras camadas, ou seja, adicionarmos outras `geom_line()`. Vamos fazer um gráfico que já conhecemos: das funções seno e cosseno:

```{r, comment=""}
# Criando os dados:
numeros <- seq(0, 100, .01)

fn_tbl <- tibble(
    sequencia = numeros,
    seno = sin(numeros),
    cosseno = cos(numeros)
)

print(fn_tbl)
```

Façamos o gráfico do seno:

```{r, comment=""}
ggplot(fn_tbl, aes(x=sequencia, y=seno)) +
    geom_line()
```

Agora vamos adicionar o cosseno como uma outra camada:

```{r, comment=""}
ggplot(fn_tbl) +
    geom_line(aes(x=sequencia, y=seno), col="forestgreen") +
    geom_line(aes(x=sequencia, y=cosseno), col="red")
```

Neste exemplo, colocamos o mapeamento `aes()` em cada uma das geometrias, isto é, cada camada tem seu próprio mapeamento `aes()`. Isto facilita separar e identificar quais são os elementos de cada camada, além de ser obrigatório para manipularmos propriamente as camadas separadamente. **Todas as geometrias aceitam uma estética independente**. Também poderíamos colocar o mapeamento do eixo X com a variável `sequencia` em `ggplot()`, pois o que muda nas camadas é o eixo Y.

```{r, comment=""}
ggplot(fn_tbl, aes(x=sequencia)) +
    geom_line(aes(y=seno), col="forestgreen") +
    geom_line(aes(y=cosseno), col="red")
```

## Histogramas

Fazemos histogramas com `geom_histogram()`.

### mpg

Vamos utilizar os dados em `mpg`.

```{r, comment=""}
glimpse(mpg)

ggplot(mpg, aes(x=displ)) +
    geom_histogram()
```

Acima, fizemos o histograma do número de cilindradas dos carros amostrados. Por default, o ggplot calcula automaticamente a quantidade e largura dos bins (o agrupamento dos dados), mas podemos mudar com `binwidth`:

```{r, comment=""}
ggplot(mpg, aes(x=displ)) +
    geom_histogram(binwidth = 10)
```

```{r, comment=""}
ggplot(mpg, aes(x=displ)) +
    geom_histogram(binwidth = .2)
```

E `bins` (a quantidade):

```{r, comment=""}
ggplot(mpg, aes(x=displ)) +
    geom_histogram(bins = 2)
```

```{r, comment=""}
ggplot(mpg, aes(x=displ)) +
    geom_histogram(bins = 10)
```

```{r, comment=""}
ggplot(mpg, aes(x=displ)) +
    geom_histogram(bins = 30)
```

Também podemos mudar o preenchimento das barras com `fill` e a cor das bordas com `color`:

```{r, comment=""}

ggplot(mpg, aes(x=displ)) +
    geom_histogram(fill="skyblue", color="tomato")
```

```{r, comment=""}
ggplot(mpg, aes(x=displ)) +
    geom_histogram()
```

### insurance

Vamos fazer outro exemplo com nossos dados `insurance.csv`.

```{r, echo=FALSE, results='hide'}
seguro <- read_csv("datasets/insurance.csv")
```

```{r, eval=FALSE}
# Carregando:
seguro <- read_csv("insurance.csv")
```

```{r, comment=""}
# Estrutura:
glimpse(seguro)
```

Vamos fazer um histograma dos valores do seguro:

```{r, comment=""}
ggplot(seguro, aes(x=charges)) +
    geom_histogram()
```

Agora, vamos criar uma sobreposição de histogramas a partir das classes de fumante, ou seja, mapearemos as cores dos preenchimentos dos histogramas para cada uma das duas classes de fumantes.

```{r, comment=""}
ggplot(seguro, aes(x=charges, fill=smoker)) +
    geom_histogram(color="white")
```

Podemos alterar os bins:

```{r, comment=""}
ggplot(seguro, aes(x=charges, fill=smoker)) +
    geom_histogram(color="white", bins=15)
```

## Gráfico de Barras

Fazemos um gráfico de barras com `geom_bar()`. Vamos usar `mpg` e ver a distribuição das montadoras:

```{r, comment=""}
ggplot(mpg, aes(manufacturer)) +
    geom_bar()
```

Podemos colorir este gráfico com `color` e `fill`:

```{r, comment=""}
ggplot(mpg, aes(manufacturer)) +
    geom_bar(color="white", fill="skyblue")
```

Podemos melhorar este gráfico: vamos ordernar de forma decrescente. Faremos isto aplicando a função `reorder()` à variável desejada.

```{r, eval=FALSE}
ggplot(
    mpg,
    aes(x=reorder(manufacturer, manufacturer, length, decreasing=TRUE))
) +
    geom_bar(color="white", fill="skyblue")
```

## Boxplot

A função para boxplots é `geom_boxplot()`. Vamos usar novamente nosso conjunto de dados `insurance.csv` e ver as diferenças de índice de massa corporal (`bmi`) entre as diferentes regiões, as quais separaremos pelo mapeamento de cores.

```{r, comment=""}
ggplot(seguro, aes(y=bmi, color=region)) +
    geom_boxplot()
```

Com os dados de `iris`, podemos criar um boxplot descrevendo o tamanho da pétala entre as diferentes espécies:

```{r, comment=""}
ggplot(iris, aes(y=Petal.Length, x=Species)) +
    geom_boxplot()
```

Podemos mudar o preenchimento mapeando cada cor a uma espécie: 

```{r, comment=""}
ggplot(iris, aes(y=Petal.Length, x=Species, fill=Species)) +
    geom_boxplot()
```

Podemos mudar o preenchimento manualmente com `scale_fill_manual()`, como abaixo:

```{r, comment=""}
ggplot(iris, aes(y=Petal.Length, x=Species, fill=Species)) +
    geom_boxplot() +
    scale_fill_manual(
        values=c("#999999", "#E69F00", "#56B4E9")
    )
```

Ou com uma paleta pré-definida, por exemplo, com `scale_fill_brewer()`:

```{r, comment=""}
ggplot(iris, aes(y=Petal.Length, x=Species, fill=Species)) +
    geom_boxplot() +
    scale_fill_brewer(palette="Dark2")
```

## Facetting

*Facetting* (em ggplot: *facet*) é o ato de dividir os dados em subconjuntos e produzir o gráfico de acordo com os subconjuntos. Há duas funções que podemos fazer isotp: `facet_wrap()` e `facet_grid()`.

Com `facet_wrap()`, dividimos os dados de acordo com um subconjunto definido por uma variável. Vamos fazer um plot subdividido com os dados de `mpg`. Primeiro, o plot:

```{r, comment=""}
ggplot(mpg, aes(x=displ, y=hwy)) +
    geom_point()
```

Fizemos um gráfico das cilindradas (`displ`) em relação às milhas por galão (`hwy`). Vamos ver esta mesma relação, agora separando pelos tipos de carro, ou seja, faremos um plot `displ ~ hwy` para cada classe de carro. Faremos isto com `facet_wrap()`.

```{r, comment=""}
ggplot(mpg, aes(x=displ, y=hwy)) +
    geom_point() +
    facet_wrap(~class)
```

O argumento `~class` significa que estamos subdividindo os dados pela variável `class`, onde o operador `~` apenas define a variável.

Com os dados de `seguro`, podemos também subdividir o gráfico, vejamos:

```{r, comment=""}
ggplot(seguro, aes(x=charges, fill=smoker)) +
    geom_histogram(color="white")
```

Este é um gráfico que já fizemos, mas agora veremos esta distribuição de acordo com as regiões amostradas.

```{r, comment=""}
ggplot(seguro, aes(x=charges, fill=smoker)) +
    geom_histogram(color="white") +
    facet_wrap(~region)
```

Vamos utilizar `facet_grid()` agora. A diferença é que criaremos um grid de gráficos, onde as colunas e linhas podem representar diferentes fatores, por isto temos os argumentos `rows` e `cols` para identificar as variáveis na estrutura do grid.

```{r, comment=""}
ggplot(seguro, aes(x=charges, fill=smoker)) +
    geom_histogram(color="white") +
    facet_grid(rows=vars(sex), cols=vars(region))
```

Este gráfico acima possui muito mais informações do que os anteriores. Em cada gráfico, temos o histograma dividido em fumantes e não-fumantes. Contudo, cada coluna representa uma região (`cols=vars(region)`) e cada linha representa um sexo amostrado (`rows=vars(sex)`).

## Editando Gráficos

Com a função `labs()`, podemos modificar os títulos do gráfico.

```{r, comment=""}
ggplot(iris, aes(y=Petal.Length, x=Species, fill=Species)) +
    geom_boxplot() +
    labs(x=NULL, y="Tamanho da Pétala")
```

> Nota: também podemos usar as funções `xlab()` e `ylab()` para mudar os títulos dos eixos.

Mudamos o nome da legenda com `guides()`:

```{r, comment=""}
ggplot(iris, aes(y=Petal.Length, x=Species, fill=Species)) +
    geom_boxplot() +
    labs(x=NULL, y="Tamanho da Pétala") +
    guides(fill=guide_legend(title="Espécie"))
```

Com `scale_x_discrete()` podemos alterar as marcações deste gráfico, pois o eixo X tem fatores (variável discreta):

```{r, comment=""}
ggplot(iris, aes(y=Petal.Length, x=Species, fill=Species)) +
    geom_boxplot() +
    labs(x=NULL, y="Tamanho da Pétala") +
    guides(fill=guide_legend(title="Espécie")) +
    scale_x_discrete(labels=NULL) # retirando marcações
```

Ou podemos alterar os nomes com uma argumento `nome_antigo = nome_novo`. Neste caso, é só por exercício, pois não faz sentido alterar o nome da espécie.

```{r, comment=""}
ggplot(iris, aes(y=Petal.Length, x=Species, fill=Species)) +
    geom_boxplot() +
    labs(x=NULL, y="Tamanho da Pétala") +
    guides(fill=guide_legend(title="Espécie")) +
    scale_x_discrete(labels=c("setosa"="ST", "versicolor"="VS", "virginica"="VG"))
```

Agora vamos para um gráfico de barras que fizemos anteriormente:

```{r, eval=FALSE}
ggplot(
    mpg,
    aes(x=reorder(manufacturer, manufacturer, length, decreasing=TRUE))
) +
    geom_bar(color="white", fill="skyblue")
```

Veja que os nomes estão juntos, podemos rotacionar estes nomes com a função `theme()`.

```{r, eval=FALSE}
ggplot(
    mpg,
    aes(x=reorder(manufacturer, manufacturer, length, decreasing=TRUE))
) +
    geom_bar(color="white", fill="skyblue") +
    theme(axis.text.x=element_text(
        angle = 270,
        hjust=.1,
        vjust=.5
    ))
```

A função `theme()` especifica diversos elementos do gráfico, como background e marcações, como acabamos de ver. Podemos aplicar temas pré-definidos em ggplot para nossos gráficos como `theme_bw()` ou `theme_classic()`.

```{r, eval=FALSE}
# Usando theme_bw():

ggplot(
    mpg,
    aes(x=reorder(manufacturer, manufacturer, length, decreasing=TRUE))
) +
    geom_bar(color="white", fill="skyblue") +
    theme_bw() +
    theme(axis.text.x=element_text(
        angle = 270,
        hjust=.1,
        vjust=.5
    )) +
    labs(x="Montadora", y="Quantidade")
```

```{r, eval=FALSE}
# Usando theme_classic():

ggplot(
    mpg,
    aes(x=reorder(manufacturer, manufacturer, length, decreasing=TRUE))
) +
    geom_bar(color="white", fill="skyblue") +
    theme_classic() +
    theme(axis.text.x=element_text(
        angle = 270,
        hjust=.1,
        vjust=.5
    )) +
    labs(x="Montadora", y="Quantidade")
```

Por fim, vamos manipular nosso gráfico de linhas das funções seno e cosseno:

```{r, comment=""}
numeros <- seq(0, 100, .01)

fn_tbl <- tibble(
    sequencia = numeros,
    seno = sin(numeros),
    cosseno = cos(numeros)
)

ggplot(fn_tbl, aes(x=sequencia)) +
    geom_line(aes(y=seno), col="forestgreen") +
    geom_line(aes(y=cosseno), col="red")
```

Primeiramente, vamos retirar o nome dos eixos:

```{r, comment=""}
ggplot(fn_tbl, aes(x=sequencia)) +
    geom_line(aes(y=seno), col="forestgreen") +
    geom_line(aes(y=cosseno), col="red") +
    labs(x=NULL, y=NULL)
```

Agora, vamos colocar uma legenda. Para isto, faremos um mapeamento da cor para uma string. Assim, dentro de `aes()` colocaremos `col="Seno"` e `col="Cosseno"`, cada um em sua respectiva camada.

```{r, eval=FALSE}
ggplot(fn_tbl, aes(x=sequencia)) +
    geom_line(aes(y=seno, col="Seno")) +
    geom_line(aes(y=cosseno, col="Cosseno")) +
    labs(x=NULL, y=NULL) +
    theme_classic()
```

Ainda não terminamos: temos um mapeamento, mas faltam as cores. A partir do mapeamento estético, definiremos as cores com a função `scale_colour_manual()` que produzirá estes elementos da legenda.

```{r, comment=""}
ggplot(fn_tbl, aes(x=sequencia)) +
    geom_line(aes(y=seno, col="Seno")) +
    geom_line(aes(y=cosseno, col="Cosseno")) +
    labs(x=NULL, y=NULL) +
    scale_color_manual(
        name="Função", 
        breaks = c("Seno", "Cosseno"),
        values = c("red", "green")
    ) +
    theme_classic()
```

Por fim, vamos manipular as escalas dos eixos. Como são eixos contínuos, vamos utilizar as funções `scale_x_continuous()` e `scale_y_continuous()`, dentro das quais forneceremos a sequência no intervalo desejado de números.

```{r, comment=""}
ggplot(fn_tbl, aes(x=sequencia)) +
    geom_line(aes(y=seno, col="Seno")) +
    geom_line(aes(y=cosseno, col="Cosseno")) +
    labs(x=NULL, y=NULL) +
    scale_color_manual(
        "Função", 
        breaks = c("Seno", "Cosseno"),
        values = c("red", "green")
    ) +
    scale_x_continuous(breaks=seq(0, 100, 5)) +
    scale_y_continuous(breaks=seq(-1, 1, 0.25)) +
    theme_classic()
```

> Nota: as funções `ylim()` e `xlim()` também definem a escala dos eixos!