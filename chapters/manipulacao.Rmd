# (PART) Manipulação de Dados {-}

# Introdução {-}

Até aqui vimos o fundamental da linguagem R: seus tipos e estruturas de dados, operadores e várias funções básicas. Após aprender o que existe em R, nosso próximo passo é entender como manipular todas estas estruturas. De uma certa forma, a programação em R começa a partir desta parte. Para entrarmos em manipulação, há duas características básicas importantes para sabermos: ordenação e início de contagem.

Em geral, as estruturas de dados em R são **ordenadas**, isto é, a posição de seus elementos possui uma ordem e podemos utilizar tal posição para acessar elementos individuais e partes específicas do objeto. Esta ordenação é definida na própria construção do objeto: é a ordem na qual colocamos cada elemento. Esta característica é extremamente importante para a indexação, que veremos em breve.

A segunda característica refere-se ao primeiro número da ordenação de um objeto. Em linguagens de programação, em geral, o primeiro elemento de qualquer objeto sempre encontra-se na posição zero, ou seja, sempre iniciamos a contagem por zero. Em R, é diferente: todas as posições iniciam-se por `1`. Assim, em todo e qualquer objeto, o primeiro elemento sempre estará na posição `1`.

Para resumir alguns dos pontos mais importantes de manipulação de dados em R, nós veremos, nos próximos capítulos:

- Indexação de estruturas de dados;
- Modificação de dados;
- Filtragem de dados;

Algumas observações desta seção do livro:

1. Todas as manipulações de vetores são iguais para qualquer tipo de dado, assim, não mostraremos sempre a mesma manipulação para todos os vetores atômicos;
1. Todas as manipulações para matrizes valem para arrays, portanto serão mostradas apenas matrizes nesta parte;
1. Focaremos principalmente em quatro estruturas: vetores, matrizes, listas e data frames.

## Conjuntos de Dados

Outro ponto importante é que, a partir desta seção, utilizaremos vários conjuntos de dados diferentes para manipularmos. Vários destes estão pré-instalados no R, enquanto outros terão de ser importados para sua sessão atual.

### Dados Pré-Instalados

A linguagem R possui dezenas de conjuntos de dados pré-instalados, prontos para uso imediato. Para saber quais são estes dados disponíveis, executamos:

```{r, eval=FALSE}
# Vendo todos os dados disponíveis em R:
data()
```

Ou com:

```{r, eval=FALSE}
# Vendo todos os dados disponíveis em R:
help(package="datasets")
```

Para tais conjuntos de dados, precisamos apenas seu nome para acessá-los. Por exemplo, vamos acessar o conjunto de dados `rivers`:

```{r, comment=""}
rivers
```

Perceba que este conjunto de dados não nos diz nada sobre ele, obviamente, pois são apenas os dados. Podemos saber as especificações de cada conjunto em sua página de ajuda:

```{r, eval=FALSE}
?rivers
```

Todos os conjuntos de dados próprios de R (e aqueles imbuídos nos pacotes) possuem página de ajuda.

Contudo, se apenas acessarmos os dados como acima, eles não estarão gravados na sessão. Para carregar um conjunto de dados, usamos a função `data()`, como abaixo.

```{r, eval=FALSE}
data(rivers)
```

A partir de agora, há uma variável chamada `rivers` na sessão atual.

### Importando Dados

Outra forma de obtermos dados é importando-os para a sessão atual de R. Para isto precisamos: do arquivo com os dados e de uma função para importar este arquivo para o R. Há diversas formas de se importar arquivos em R, com funções do próprio R e funções de outros pacotes específicos que devem ser instalados previamente à importação.

Neste livro, quando preciso importar arquivos, usaremos apenas arquivos com formato `.csv` (separado por vírgulas). Estes arquivos são bem simples e sua importação é fácil e sem muitos problemas, quando os há. Arquivos Excel (formato `.xlsx` ou `.xls`) requerem pacotes externos e muitas vezes dependem de sua versão do R e de outros programas em seu computador, como Java. Por estes motivos, sempre usaremos arquivos `.csv`, mas se você quiser saber como importar arquivos Excel, procure pacotes como `readxl` e `xlsx`, que são específicos para este tipo de arquivo.

Os dados externos utilizados neste livro estarão no GitLab, neste [link](https://gitlab.com/aprenda/datasets). Vamos ver como importar um arquivo em três passos, utilizando o arquivo `Botan.csv`, o qual está na pasta do GitLab.

1. Faça o download do arquivo para seu diretório de trabalho atual.
1. Com a função `read.csv()`, importamos um arquivo do tipo `.csv` para o R. Coloque o nome do arquivo nesta função para importá-lo.
1. Crie uma variável para gravar o conjunto de dados e dar um nome a ele.

Os dois últimos passos são demonstrados abaixo, conjuntamente.

```{r, eval=FALSE}
botan <- read.csv("Botan.csv")
```

```{r, eval=FALSE}
botan <- read.csv("~/Dropbox/Disciplina-R/datasets/Botan.csv")
```

::: {.infobox .exclamacao data-latex="{exclamacao}"}
**Datasets**<br><br>
Quer brincar com vários conjuntos de dados? Visite o site [Kaggle](https://www.kaggle.com/datasets/). Este site possui muitos conjuntos de dados livres para uso que são disponibilizados pela comunidade. Há dados de dezenas de assuntos. Procure algum que mais tenha lhe interessa, faça o download e treine manipulação (ou análise) de dados!
:::