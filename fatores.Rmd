# Fatores

Muitas vezes, trabalharemos com objetos que são tratados categoricamente. Estatisticamente: variáveis categóricas (ou nominais). A linguagem R possui um tipo específico para este tipo de dado: fatores.

Fatores são vetores com a adição de dois atributos: `class` e `levels`. Portanto, talvez possam ser descritos como *vetores com informações adicionais*, mas qualquer outra estrutura de dados (além de vetor) também se enquadraria nesta definição. Uma definição mais adequada para fatores seria: **um vetor que possui apenas valores pré-definidos** — os níveis do fator. Estes níveis descrevem os elementos do fator; os categorizam.

Vamos criar e entender detalhadamente sobre esta estrutura neste capítulo.

## Fatores Nominais

Obrigatoriamente, temos de trabalhar em termos categóricos. Portanto, suponha que estamos trabalhando com sabores e tenhamos as categorias `Doce`, `Amargo` e `Ácido`. E possuímos os seguintes dados:

```{r, comment=""}
sabor <- c(
    "Amargo", "Ácido", "Amargo", "Doce",
    "Ácido", "Doce", "Amargo", "Doce",
    "Ácido", "Amargo"
)

sabor
```

Temos, então, um vetor de catacteres normal. Contudo, não representa as informações como queremos; não há interpretação alguma de que são fatores e R entenderá somente como um vetor normal. Temos de transformar isto em um fator. Faremos esta tranformação com a função `factor()`. Ela possui a seguinte sintaxe básica:

```{r, eval=FALSE}
factor(x, levels, ordered)
```

Onde:

- `x`: o vetor; os dados que constituirão o fator;
- `levels`: os níveis do fator;
- `ordered`: valor booleano indicando se é um fator ordenado ou não.

Podemos aplicar esta função no vetor:

```{r, comment=""}
sabor <- factor(sabor)

sabor
```

Ou criar um fator diretamente:

```{r, comment=""}
sabor <- factor(
    c(
        "Amargo", "Ácido", "Amargo", "Doce",
        "Ácido", "Doce", "Amargo", "Doce",
        "Ácido", "Amargo"
    )
)

sabor
```

Detalhe: nos exemplos acima, apenas aplicamos afunção `factor()` ao vetor e ela, automaticamente, identificou os níveis de acordo com os elementos do vetor. Contudo podemos também especificar os níveis de um dado fator através do argumento `levels`. Como abaixo:

```{r, comment=""}
sabor <- factor(
    c(
        "Amargo", "Ácido", "Amargo", "Doce",
        "Ácido", "Doce", "Amargo", "Doce",
        "Ácido", "Amargo"
    ),
    levels=c("Ácido", "Amargo", "Doce")
)

sabor
```

A diferença é que nem sempre todos os níveis de um vetor estarão presentes na amostragem, isto é, nos seus dados. Assim, podemos especificar para o R que existem níveis ausentes nos dados.

Perceba, no print do nosso fator `sabor`, acima:

- O print da variável muda. A variável agora aparece com o dizer: `Levels: Ácido Amargo Doce`, significando que é um fator e estes são seus níveis.
- Em seu ambiente no RStudio, a variável `sabor` era descrita como `chr`, pois era um vetor de caracteres. Agora, é representada como `Factor w/ 3 levels`, pois é um fator.

Vamos verificar as carcterísticas deste fator: vejamos seus atributos.

```{r, comment=""}
attributes(sabor)
```

Temos, de fato, os atributos `class`, que nos diz que `sabor` é um fator (`factor`); e `levels` nos diz os níveis de `sabor`.

Além de `attributes()`, podemos acessar as informações de um fator com as funções `class()` e `levels()`:

```{r, comment=""}
# Acessando a classe do fator:
class(sabor)
```

```{r, comment=""}
# Acessando os níveis do fator:
levels(sabor)
```

Uma importante distinção: perceba, no código acima, que nossos fatores são `"Ácido"`, `"Amargo"` e `"Doce"`. Estes níveis não possuem ordenação; ordem dos níveis não é algo importante para este fator. Assim, diremos que é um fator puramente nominal (ou **fator nominal**). Na próxima seção conheceremos os fatores ordenados.

## Fatores Ordenados

Fatores ordenados são fatres nos quais a **ordenação dos níveis é importante**; a ordem dos níveis possui algum significado. Vamos criar um fator ordenado.

Neste exemplo, suponha que estamos trabalhando com o nível de cianotoxinas de diversos lagos e nossos níveis são: `Alto`, `Médio` e `Baixo`. Claramente a ordem importa, pois indica uma hierarquia da eutrofização do lago.

Há dois modos de se criar um fator ordenado. Com o argumento `òrdered=TRUE` em `factor()`:

```{r, comment=""}
lagos <- factor(
    c(
        "Alto", "Médio", "Baixo", "Alto", "Baixo",
        "Baixo", "Baixo", "Alto", "Alto", "Baixo"
    ),
    levels=c("Baixo", "Médio", "Alto"),
    ordered=TRUE
)

lagos
```

Ou pela função `ordered()`:

```{r, comment=""}
lagos <- ordered(
    c(
        "Alto", "Médio", "Baixo", "Alto", "Baixo",
        "Baixo", "Baixo", "Alto", "Alto", "Baixo"
    ),
    levels=c("Baixo", "Médio", "Alto")
)

lagos
```

Perceba, no print do nosso fator `lagos`, acima:

- O print da variável muda. A variável agora aparece com o dizer: `Levels: Baixo < Médio < Alto`, significando que é um fator e estes são seus níveis, nesta ordem.
- Em seu ambiente no RStudio, a variável `lagos` é representada como `Ord. factor w/ 3 levels`, pois é um fator ordenado.

Vamos verificar as carcterísticas deste fator: vejamos seus atributos.

```{r, comment=""}
attributes(lagos)
```

Apareceu uma nova classe: `ordered`. Portanto, nossa variável é um fator e é ordenado.

Um último detalhe: a ordenação vem da nossa definição! Veja no nosso último código acima, o argumento `levels` é:

```{r, eval=FALSE}
levels=c("Baixo", "Médio", "Alto")
```

E se nós o mudássemos?

```{r, comment=""}
lagos <- ordered(
    c(
        "Alto", "Médio", "Baixo", "Alto", "Baixo",
        "Baixo", "Baixo", "Alto", "Alto", "Baixo"
    ),
    levels=c("Alto", "Baixo", "Médio")
)

lagos
```

```{r, comment=""}
attributes(lagos)
```

Agora nosso fator (ainda ordenado) mudou a hierarquia de seus níveis:

> Levels: Alto < Baixo < Médio

O que está errado. Isto porque mudamos o argumento `levels` para:

```{r, eval=FALSE}
levels=c("Alto", "Baixo", "Médio")
```

## Fatores Internamente

Nas seções anteriores, criamos um fator nominal `sabor` e um faotr ordenado `lagos`. Vejamos seus tipos:

```{r, comment=""}
typeof(sabor)
typeof(lagos)
```

Ambos fatores são do tipo `integer`.

Através da simples visualização de fatores, pode nos parecer, erroneamente, que estamos lidando com vetores de caracteres, uma vez que os dados são caracteres. Entretanto, R nos os interpreta assim.

Internamente, R interpreta fatores como números, especificamente, inteiros. Isto porque fatores são construídos com vetores numéricos do tipo `integer`. Tecnicamente, R faz um mapeamento dos níveis do fator com os números inteiros. Desta forma, fatores são, na verdade, um array com os números representando os dados e outro array com os nomes que são maepados aos números.

Podemos ver isto claramente com a função `str()`, que nos mostra a estrutura de um objeto em R:

```{r, comment=""}
str(sabor)
```

Internamente, o fator `sabor` é visto como:

> 2 1 2 3 1 3 2 3 1 2

Estes números representam os níveis do fator. Vamos relembrar como R está vendo os níveis deste fator:

```{r, comment=""}
levels(sabor)
```

Agora comparemos diretamente ambas visualizações deste fator:

> Amargo Ácido Amargo Doce Ácido Doce Amargo Doce Ácido Amargo

> 2 1 2 3 1 3 2 3 1 2

É claro que: `2` é `Amargo`; `1` é `Ácido`; `3` é `Doce`. Assim como a ordem expressa em `levels(sabor)`.

::: {.infobox .exclamacao data-latex="{exclamacao}"}
**Cuidado!**<br><br>
O fator `sabor` não é ordenado! R apenas pegou a ordem que definimos no fator para simbolizar a ordem dos números internamente.
:::

Podemos fazer o mesmo com o fator `lagos`. Vamos relembrar os níveis deste fator:

```{r, comment=""}
levels(lagos)
```

E como R está vendo `lagos` internamente:

```{r, comment=""}
str(lagos)
```

Agora comparemos ambas visualizações deste fator:

> Alto Médio Baixo Alto Baixo Baixo Baixo Alto Alto Baixo

> 3 2 1 3 1 1 1 3 3 1

Portanto: `3` é `Alto`; `2` é `Médio`; `1` é `Baixo`. Assim como a ordem definida em `lagos`, que é `Baixo < Médio < Alto`.

Nesta seção, pudemos ver claramente que nossos dados, quando fatores, são recodificados para números e R trabalha com estes números nas análises. Contudo, cuidado! Este é um comporatmento geral, mas há várias exceções! Dependendo da função, ela tratará/transformará fatores para caracteres ou tentarão transformar e resultarão em erros. Então, saiba como sua função utiliza fatores.

## Exercícios

::: {.infobox .exclamacao data-latex="{exclamacao}"}
**Tente Sozinho!**<br><br>
Sempre que possível, primeiro tente resolver os exercícios apenas olhando seus códigos, sem executá-los. Depois de pensar no resultado, execute os códigos e veja se acertou.
:::

::: {.infobox .exclamacao data-latex="{exclamacao}"}
**Pesquise!**<br><br>
Uma das características de um programador é saber procurar respostas para os problemas em seu código e afins. Não sabe como responder alguma das questões? Pesquise sobre o tema em questão (na internet, na documentação oficial, ...) e aprenda mais sobre ele!
:::

### Questão 1 {-}

Crie um fator a partir do vetor dado abaixo, com os níveis `Claro` e `Escuro`.

```{r, eval=FALSE}
futuro_fator <- c(4, 16, 4, 4, 4, 16, 4, 16, 16, 4)
```

### Questão 2 {-}

Transforme o fator abaixo em um vetor e responda: que tipo de vetor é?

```{r, eval=FALSE}
futuro_vetor <- factor(
    c(
        "Sim", "Sim", "Não", "Sim", "Não",
        "Não", "Sim", "Não", "Sim", "Não"
    ),
    levels=c("Sim", "Não")
)
```

### Questão 3 {-}

Fatores podem conter tipos de dados diferentes?

## Respostas

### Questão 1 {-}

Primeiro: lembre-se que o default do R é criar `doubles`. Então vamos transformar em um vetor de `integer`.

```{r, comment=""}
futuro_fator <- c(4, 16, 4, 4, 4, 16, 4, 16, 16, 4)

futuro_fator <- as.integer(futuro_fator)
```

Agora colocaremos os atributos necessários: `levels` e `class`.

```{r, comment=""}
levels(futuro_fator) <- c("Claro", "Escuro")
class(futuro_fator) <- "factor"
```

E agora vejamos seus atributos:

```{r, comment=""}
attributes(futuro_fator)
```

E verificando se realmente é interpretado um fator:

```{r, comment=""}
is.factor(futuro_fator)
```

### Questão 2 {-}

```{r, comment=""}
futuro_vetor <- factor(
    c(
        "Sim", "Sim", "Não", "Sim", "Não",
        "Não", "Sim", "Não", "Sim", "Não"
    ),
    levels=c("Sim", "Não")
)
```

Para transformar de volta em um vetor, temos apenas que retirar seus atributos:

```{r, comment=""}
class(futuro_vetor) <- NULL
levels(futuro_vetor) <- NULL
```

Único cuidado: temos que retirar o atributo `class` primeiro e depois o atributo `levels`.

Agora podemos ver que `futuro_vetor` é um vetor de inteiros:

```{r, comment=""}
futuro_vetor
```

```{r, comment=""}
typeof(futuro_vetor)
```

Isto se dá porque fatores são recodificados em números inteiros. Ao apagarmos os atributos, apagamos o mapeamento dos nomes dos níveis para com os números. Então, sobrou apenas o array de números.

### Questão 3 {-}

Não. Fatores são construídos a partir de vetores, então podem conter apenas um tipo de dado. Para provarmos, vamos modificar nosso fator `sabor`, colocando um número junto aos níveis:

```{r, comment=""}
sabor <- factor(
    c(
        "Amargo", "Ácido", "Amargo", "Doce",
        "Ácido", "Doce", "Amargo", "Doce",
        "Ácido", 1
))
```

E vejamos seus atributos:

```{r, comment=""}
attributes(sabor)
```

Vemos que o número `1` foi transformado em um dos níveis, resultando no total de quatro níveis para este fator. POdemos tabmém ver a estrutura do fator:

```{r, comment=""}
str(sabor)
```

Onde vemos que `1` foi mapeado para o número `1` e os outros níveis vão de `2` a `4`.
