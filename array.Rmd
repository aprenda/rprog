# Arrays

Arrays são vetores com a adição do atributo `dim`, ou seja, "vetores" com uma ou mais dimensões. Geralmente, quando falamos sobre arrays, falamos de arrays multidimensionais, justamente por causa deste atributo e porque os utilizamos quando necessitamos de mais dimensões.

Neste capítulo, conheceremos um pouco mais sobre estas estruturas.

## Criando Arrays

Como são criados a partir de vetores, arrays também podem conter apenas um tipo de dado. Vamos criar nossos primeiros arrays. Fazemos isto com a função `array()`, a qual possui a seguinte sintaxe:

```{r, eval=FALSE}
array(data, dim, dimnames)
```

Onde:

- `data`: os dados do array.
- `dim`: um vetor numérico especificando a quantidade de elementos em cada dimensão.
- `dimnames`: nome de cada dimensão.

Vamos exemplificar criando arrays de dimensões 1 a 3.

### Array unidimensional (1D)

```{r, comment=""}
arr1D <- array(1:64, dim=64)

arr1D
```

Em `arr1D`, especificamos que o dado é um vetor numérico contendo os números de `1` a `64`; e especificamos também que na primeira dimensão caberão os 64 elementos. Deste modo, `arr1D` possui uma única dimensão e, portanto, é unidimensional.

Seus atributos são:

```{r, comment=""}
attributes(arr1D)
```

E de fato é um array:

```{r, comment=""}
is.array(arr1D)
```

### Array bidimensional (2D)

```{r, comment=""}
arr2D <- array(1:63, dim=c(7, 9))

arr2D
```

Em `arr2D`, especificamos que o dado é um vetor numérico contendo os números de `1` a `63`; e especificamos também que:

- Na primeira dimensão (linhas) caberão 7 elementos;
- Na segunda dimensão (colunas) caberão 9 elementos.

Portanto, de certa forma, criamos uma matriz com 7 linhas e 9 colunas (uma matriz 7x9). Sendo assim, `arr2D` possui duas dimensões (linhas e colunas) e é bidimensional.

::: {.infobox .exclamacao data-latex="{exclamacao}"}
A estrutura de dados **matriz** será vista no próximo capítulo em detalhes, mas esta nomenclatura já é colocada aqui pois facilita a compreensão de arrays.
:::

Seus atributos são:

```{r, comment=""}
attributes(arr2D)
```

E de fato é um array:

```{r, comment=""}
is.array(arr2D)
```

### Array tridimensional (3D)

```{r, comment=""}
arr3D <- array(1:64, dim=c(4, 4, 4))

arr3D
```

Em `arr3D`, especificamos que o dado é um vetor numérico contendo os números de `1` a `64`; e especificamos também que:

- Na primeira dimensão (linhas) caberão 4 elementos;
- Na segunda dimensão (colunas) caberão 4 elementos;
- Na terceira dimensão caberão 4 elementos.

A visualização de objetos em três dimensões já começa ser mais difícil. Veja o print de `arr3D` novamente: você verá que existem os seguintes símbolos:

- `, , 1`
- `, , 2`
- `, , 3`
- `, , 4`

Que simbolizam a posição na terceira dimensão.

Contudo, podemos entender com mais facilidade, não pensando em dimensões, mas pensando em matrizes. Pense que, com o código:

```{r, eval=FALSE}
array(1:64, dim=c(4, 4, 4))
```

Estamos criando 4 matrizes 4x4. Para simplificar, veja este outro array 3D:

```{r, comment=""}
arr3D_2 <- array(1:60, dim=c(5, 4, 3))

arr3D_2
```

Executando o código:

```{r, eval=FALSE}
array(1:60, dim=c(5, 4, 3))
```

Criamos 3 matrizes com 5 linhas e 4 colunas.

Seus atributos são:

```{r, comment=""}
attributes(arr3D)
attributes(arr3D_2)
```

E de fato são um arrays:

```{r, comment=""}
is.array(arr3D)
is.array(arr3D_2)
```

Um último detalhe: veja no print do array `array3D_2` que nosso dado é o vetor numérico `1:60` e que os números em cada matriz são preenchidos até acabar as vagas em cada dimensão. Por isto, os dados de cada matriz segue a sequência dos números de `1` a `60`.

## Nomes e Dimensões

A função `array()` tem um argumento opcional: `dimnames`. Este argumento, quando utilizado, nomeia as dimensões do array. Vamos utilizar nosso `arr3d_2` e nomear suas dimensões.

Para utilizar este argumento, utilizaremos a função `list()` com os nomes de cada dimensão. Primeiramente, vamos explicar o que são os nomes de cada dimensão, neste caso de um array 3D:

- Primeira dimensão: linhas de cada matriz. Estamos nomeando cada linha de cada matriz nas três dimensões.
- Segunda dimensão: colunas de cada matriz. Estamos nomeando cada coluna de cada matriz nas três dimensões.
- Terceira dimensão: nomes de cada matriz. Estamos nomeando cada matriz nas três dimensões. Este argumento substituirá o número em, por exemplo, `, , 1`.

Para facilitar, devemos criar separadamente estes nomes em vetores diferentes. Criaremos, então:

```{r, comment=""}
linhas <- c("L1", "L2", "L3", "L4", "L5")
colunas <- c("C1", "C2", "C3", "C4")
matrizes <- c("M1", "M2", "M3")
```

Criamos cada vetor de caracteres seguindo os atributos de `array3D_2`:

- `linhas` possui 5 nomes, pois `array3D_2` possui 5 elementos na primeira dimensão (as linhas).
- `colunas` possui 4 nomes, pois `array3D_2` possui 4 elementos na segunda dimensão (as colunas).
- `matrizes` possui 3 nomes, pois `array3D_2` possui 3 elementos na terceira dimensão (as matrizes).

Agora, basta que criemos novamente o array, fornecendo estes três vetores dentro da função `list()` (e nesta ordem), como abaixo.

```{r, comment=""}
arr3D_2 <- array(
    1:60,
    dim=c(5, 4, 3),
    dimnames=list(linhas, colunas, matrizes)
)

arr3D_2
```

Agora nossas matrizes estão totalmente nomeadas.

## Exercícios

::: {.infobox .exclamacao data-latex="{exclamacao}"}
**Tente Sozinho!**<br><br>
Sempre que possível, primeiro tente resolver os exercícios apenas olhando seus códigos, sem executá-los. Depois de pensar no resultado, execute os códigos e veja se acertou.
:::

::: {.infobox .exclamacao data-latex="{exclamacao}"}
**Pesquise!**<br><br>
Uma das características de um programador é saber procurar respostas para os problemas em seu código e afins. Não sabe como responder alguma das questões? Pesquise sobre o tema em questão (na internet, na documentação oficial, ...) e aprenda mais sobre ele!
:::

### Questão 1 {-}

Diferencie os objetos abaixo, se houver diferença.

```{r, comment=""}
obj1 <- c(1, 2, 3, 4)
obj2 <- array(1:4)
```

### Questão 2 {-}

Transforme o vetor abaixo em um array 2D. APós, tranforme este array 2D em um array 3D no qual, em cada dimensão, há uma matriz 5x5.

```{r, eval=FALSE}
futuro_array <- 1:125
```

### Questão 3 {-}

Nomeie cada dimensão do array `futuro_array` que criamos na questão anterior.

### Questão 4 {-}

Explique o que está ocorrendo no código abaixo e construa corretamente o array `arr` em três dimensões para que não haja este comportamento.

```{r, comment=""}
vet1 <- 1:10
vet2 <- 11:15

arr <- array(c(vet1, vet2), c(4, 4, 2))
arr
```

## Respostas

### Questão 1 {-}

Ambos são objetos unidimensionais, mas são diferentes. Arrays unidimensionais **não são vetores**! Vetores não possuem atributos, um array possui o atributo `dim`. Veja:

```{r, comment=""}
attributes(obj1)
attributes(obj2)
```

Portanto, `obj1` é um vetor e `obj2` é um array 1D.

Se ainda estiver em dúvidas, basta verificar como o próprio R interpreta cada um:

```{r, comment=""}
is.vector(obj1)
is.vector(obj2)
```

Acima, apenas `obj1` é um vetor.

```{r, comment=""}
is.array(obj1)
is.array(obj2)
```

Acima, apenas `obj2` é um array.

### Questão 2 {-}

```{r, comment=""}
futuro_array <- 1:125
```

```{r, comment=""}
# transformando em 2D:
dim(futuro_array) <- c(25, 5)

futuro_array
```

```{r, comment=""}
# Tranformando o 2D em 3D:
dim(futuro_array) <- c(5, 5, 5)

futuro_array
```

```{r, comment=""}
# Vendo os atributos:
attributes(futuro_array)
```

```{r, comment=""}
# Verificando o array:
is.array(futuro_array)
```

### Questão 3 {-}

```{r, comment=""}
# Criando os nomes:
linhas <- c("L1", "L2", "L3", "L4", "L5")
colunas <- c("C1", "C2", "C3", "C4", "C5")
matrizes <- c("M1", "M2", "M3", "M4", "M5")
```

E, para colocar os nomes no array, podemos usar diretamente a função `dimnames()`:

```{r, comment=""}
dimnames(futuro_array) <- list(
    linhas, colunas, matrizes
)

futuro_array
```

### Questão 4 {-}

Nosso array está sendo preenchido por reciclagem de vetores.

Perceba as dimensões do array especificadas no código: `c(4, 4, 2)`, isto é, 2 matrizes 4x4. Agora veja o tamanho dos nossos vetores:

```{r, comment=""}
length(vet1)
length(vet2)
```

O vetor `vet1` tem 10 elementos enquanto `vet2` tem 5. 15 elementos ao total. NO entanto, `arr` tem de ter 32 elementos ao total, pois $4 \times4\times2=32$. Como o conjunto de dados fornecido (os vetores) não possuem número suficiente de elementos, R os recicla até que todo o array seja preenchido.

Para corrigir, temos de acertar o número de elementos em cada vetor.

```{r, comment=""}
vet1 <- 1:16
vet2 <- 17:32

arr <- array(c(vet1, vet2), c(4, 4, 2))
arr
```
