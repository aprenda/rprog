# (PART) Introdução {-}

# Conhecendo o R

Sempre que aprendemos algo novo, é interessante saber quais foram suas motivações; as ideias das quais foram originadas ou quais problemas tentam resolver. Com linguagens de programação não é diferente. Neste capítulo, veremos como e por que R foi criado, assim como suas características principais.

## História

A partir da criação dos computadores modernos a ciência percebeu à sua disposição um amplo ferramental nesta nova área. Com a velocidade dos cálculos computacionais, se fez desnecessário as montanhas de cadernos escritos à lápis repletos de cálculos feitos à mão e calculadoras de mesa; assim como os cálculos a priori de cada caso específico das distribuições estatísticas teóricas — as famigeradas tabelas de valores críticos. Agora uma quantitade gigantesca de dados poderia ser analizada em poucos minutos; os modelos matemáticos poderiam aumentar em complexidade. Neste ensejo, linguagens de programação específicas para a computação científica foram criadas — seu maior expoente na época sendo a linguagem [Fortran](https://en.wikipedia.org/wiki/Fortran) —, da mesma forma que atualmente existem sistemas operacionais específicos para fins científicos, como, por exemplo, Bio-Linux e Scientific Linux. Aqui começa a história do software R, pois entre as várias linguagens para computação numérica, estava a [linguagem S](https://en.wikipedia.org/wiki/S_(programming_language)).

Desenvolvida inicialmente na metade da década de 1970 principalmente por John Chambers, a linguagem S foi uma linguagem focada em análises estatísticas. Enquanto as análises feitas na época eram predominantemente feitas em C e Fortran, S objetivou ser mais acessível e prática, ou, nas palavras do prório desenvolvedor, "com o objetivo de programar com dados". A empresa então chamada Bell Labs Innovations (atualmente [Nokia Bell Labs](https://en.wikipedia.org/wiki/Bell_Labs)), aonde o S foi concebido, foi também a responsável por diversas outras importantes linguagem de programação como [B](https://en.wikipedia.org/wiki/B_(programming_language)), [C](https://en.wikipedia.org/wiki/C_(programming_language)) e [C++](https://en.wikipedia.org/wiki/C%2B%2B). Para seguir o modelo de designação destas linguagens, e por causa da palavra *Statistical*, a letra S foi escolhida para nomear a nova linguagem criada.

No fim da década de 1980, diversas mudanças críticas foram feitas na linguagem S, culminando na divisão temporal da linguagem: agora estava em voga o "Novo S" (*New S*) em contraposição ao "Velho S" (*Old S*). Para apresentar todas as novas funcionalidades e propriedades do Novo S, foi publicado o livro The New S Language, sendo a referência para a linguagem. Durante os anos 1980, Bell Labs Innovations disponibilizou ao código-fonte da linguagem para todos, mas a licença exclusiva da linguagem S foi adquirida pela companhia MathSoft Corporation em 1993, a qual é detentora dos direitos até hoje. Com a exclusividade de direitos sobre o produto e o nome S, esta empresa criou o software pago S-PLUS, software que continua ativo atualmente.

No ano de 1991, os estatísticos Ross Ihaka e Robert Gentleman iniciaram na Universidade Auckland, Nova Zelândia, a implementação de uma nova linguagem. Esta implementação também seria focada em análises estatísticas, além de ser fortemente baseada nas linguagem S e Scheme — linguagens das quais os autores retiraram e combinaram diferentes características. A decáda de 1990 foi a pedra fundamental deste novo projeto: após as primeiras implementações feitas pelos dois pesquisadores, o projeto foi publicamente apresentado e nomeado R — uma brincadeira com a linguagem S e uma auto-homenagem dos próprios autores, Ross e Robert. Nos anos seguintes, o número de contribuidores à nova linguagem aumentou significativamente e foi estabelecido um grupo de desenvolvedores principais da linguagem R — o *R Development Core Team*. Nesta mesma década, a linguagem R tornou-se um software livre, ou seja, suas diretrizes respeitam as quatro liberdades essenciais, sendo então livremente distribuído sob a Licença Pública Geral GNU (*GNU General Public License*).

Então, objetivamente, R é todo um sistema de análise e visualização de dados: ele inclui a linguagem em si, com sua própria sintaxe e propriedades, além de um software contendo um diverso ferramental abarcando escrita de scripts, depuração e gráficos.

## R e Outras Linguagens

Uma das principais razões de se usar R é por esta ser uma linguagem livre. Por seguir as quatro liberdades, todo o código-fonte está disponível para qualquer pessoa que esteja interessada em apenas ver ou até modificá-lo ao seu bel prazer, para então disponibilizar sua própria versão do código. Resumidamente, você não só pode tê-lo gratuitamente, assim como pode copiá-lo, modificá-lo e redistribuí-lo sem problema algum. Essa é a filosofia dos softwares livres.

Ao pensarmos em softwares livres, uma grande quantidade de pessoas imediatamente desprezam-nos, provavelmente alegando superioridade de performance e funcional por softwares não-livres. De fato, há diversas outras linguagens também específicas à área matemática e científica amplamente conhecidas e atestadas no mercado, tais como Mathematica, MatLab, Statistica e o próprio S-PLUS. Contudo, o R, além de ser gratuito e livre, não fica atrás de seus concorrentes pagos em sua funcionalidade. Há uma comunidade gigantesca provendo frequentes atualizações funcionais, ou seja, criando pacotes, os quais contém novas funções, formas de análise ou visualização/produção de gráficos. O repositório oficial de pacotes do R, chamado CRAN (acrônimo para *Comprehensive R Archive Network*), possui centenas de milhares destes pacotes que estão disponíveis a todos usuários da linguagem. Deste modo, R tornou-se uma ferramenta primordial em várias áreas, por exemplo, na ecologia, bioinformática e estatística, com cientistas criando funcionalidades específicas para cada área. Por ser tão importante e acessível, há uma predileção ao uso do R e, consequentemente, uma ação subconsciente de este se tornar a opção default de análise e visualização estatística. Quanto à performance, o R consegue manipular uma grande quantidade de dados, um dos motivos pelos quais ele é utilizado também na área de Big Data.

Como qualquer outra linguagem, o R também possui diversos contras. Primeiramente, uma característica que pode parecer um dos prós para muitas pessoas: R não é uma linguagem para programadores. Explico: R é uma **linguagem de domínio específico** (*domain-specific language*), criada especificamente para fins matemáticos, especificamente, estatísticos. Sendo tal, agrupa-se com seus pares anteriormente citados (Mathematica, MatLab, Statistica, S-PLUS) e difere-se das **linguagens de programação geral**, tais como C, C++, Java e Python. A disparidade jaz na finalidade e funcionalidade: linguagens matemáticas possuem um enfoque em análises matemáticas e visualização de dados, enquanto linguagens de programação geral não possuem um foco centralizado. Exemplo: utilizando-se a linguagem Python, assim como no R, é possível analisar grande quantidades de dados e visualizá-los — tanto é que Python é uma das linguagens mais buscadas para Big Data e Machine Learning. Entretanto, esta não é a finalidade da linguagem, apenas uma de suas funcionalidades. O próprio [site oficial](https://www.r-project.org) do R afirma que este é um projeto para computação estatística: *The R Project for Statistical Computing*. O foco do R encontra-se na análise de dados e seu ferramental, não em programação em si como praticada em outras linguagens. Pragmaticamente: com uma linguagem de programação geral, é possível construir um R, mas com o R não construímos uma linguagem de programação geral. Lembrando: os criadores do R foram dois estatísticos, e não programadores; além de esta linguagem ser baseada em outra linguagem matemática dos anos 1960.

De fato, a linguagem R talvez é muito mais interessante a uma pessoa sem um background anterior em programação (e com o objetivo específico de análises estatísticas) do que para um programador experiente. Geralmente, os problemas daqueles que usam o R baseam-se nos dados. Um programador normalmente está interessado em questões além da pura análise de modelos matemáticos. O que não impede de maneira alguma que haja complementação entre estas ferramentas: é plenamente possível unir R e Python, por exemplo, e realizar uma determinada tarefa que envolva dois objetivos separados, tal como construir um ferramental orientado ao objeto e aplicá-lo (com Python) e analisar seus resultados (com R). Um contra da linguagem R nesta mesma linha é que, comparada com outras linguagens, seu manejo de memória, velocidade de processamento e eficiência são alguns dos desafios que os desenvolvedores enfrentam e tentam melhorar constantemente.

Enfim, todo o ambiente R (a linguagem e o software), mesmo com seus contras (como qualquer outra linguagem de programação), ainda é altamente desejável por suas aplicabilidades, facilidades de uso e comunidade. Possuindo grande especificidade de usos em diversas áreas do conhecimento, graças às suas respectivas comunidades, a utilização do R apresenta-se como uma grande — quiçá melhor — alternativa para problemas que envolvam o uso direto de análise de dados.

## Principais Características

### Alto Nível

R é uma linguagem de alto nível. O que isto quer dizer, exatamente? Grosseiramente, é o quão distante estamos da linguagem da própria máquina; do hardware. Ao falarmos de níveis, nos referimos ao nível de *abstração* da linguagem. Se programamos em uma língua de menor nível (como a linguagem C, por exemplo), temos de nos ater a diversos detalhes e minúcias da arquitetura do computador, pois estamos, de uma certa forma, lidando diretamente com ela. Por outro lado, isto significa um controle muito maior sobre a máquina. Ao programarmos em uma linguagem de nível mais alto, não precisamos sequer conhecer tais minúcias e especificações do hardware, pois a própria linguagem lida e toma decisões internamente, ou seja, estas decisões são *abstraídas* do programador. Assim, a ideia do nível de uma linguagem é comparativa: uma linguagem é de maior nível comparativamente a outra linguagem.

Pragmaticamente, em linguagens de alto nível pensamos na *ideia* do que queremos fazer e como fazê-lo sem entrar em detalhes; em linguagens de baixo nível, pensamos nos *detalhes* da execução e como o computador irá lidar com nosso programa. Assim, características de linguagens de alto nível, são, geralmente:

1. Comandos mais explícitos e mais fáceis;
1. Maior legibilidade do código;
1. Menos código necessário para completar-se ações;
1. Consequentemente, menores scripts escritos.

Pense na seguinte analogia: queremos fazer uma torrada. Em uma linguagem de alto nível, nossa sequência de comandos seria algo como:

1. Pegue um pão;
1. Insira-o na torradeira;
1. Retire-o quando pronto.

Em uma linguagem de menor nível, teríamos muito mais com o que nos preocuparmos:

1. Pegue um pão;
1. Verifique se o tipo de pão é adequado para a torradeira;
1. Vá até a torradeira;
1. Coloque o pão na posição vertical;
1. Insira-o na torradeira;
1. Verifique qual a voltagem da casa;
1. Verifique se a voltagem da torradeira é compatível com a da casa;
1. Ligue a torradeira na tomada;
1. Aguarde 4 minutos e 37 segundos;
1. Retire a torradeira da tomada;
1. Retire o pão da torradeira.

Um pouco mais trabalhoso, não é?

### Multi-paradigmas {#multi_paradigma}

Há os chamados [paradigmas da programação](https://en.wikipedia.org/wiki/Programming_paradigm), os quais definem diferentes modos, ou estilos, de programação. Abaixo, resumimos três paradigmas que nos são mais importantes.

- **Procedural**: A programação procedural baseia em uma série de comandos na ordem do script (de cima para baixo), instruindo a máquina passo-a-passo a realizar uma ação.
- **Funcional**: O principal elemento da programação funcional é são as funções — de onde vem o nome do paradigma. Aqui, focamos em construir funções para fins específicos: cada uma fará, idealmente, apenas uma ação de forma otimizada. Através de vários retornos destas funções, teremos o resultado final do programa.
- **Orientado ao objeto (OOP)**: Neste paradigma temos *objetos* — entidades criadas a partir de *classes* e embuídas de ações (funções) e características (atributos). No desenvolvimento do programa, um conjunto de objetos realiza as operações necessárias para obtermos o resultado final.

Diversas linguagens são feitas propriamente com um tipo único de paradigma, por exemplo, C, Pascal e BASIC utilizam o paradigma procedural como abordagem padrão. Outras linguagens, como C++ e Python, são ditas **multi-paradigmas**, pois permitem que sejam utilizados diversos paradigmas. Neste sentido, R é uma linguagem multi-paradigma, pois podemos criar programas através de funções ou códigos diretos. E sobre OOP? R tem uma forte base orientada ao objeto. Contudo, R é principalmente funcional. Esclareceremos em detalhes após falarmos sobre funções. Por enquanto, basta sabermos que **tudo em R é um objeto**.

### Interpretada

R é uma linguagem interpretada. Basicamente, significa que R executa diretamente o código do console  cada linha do script sem necessidade de um compilador. O código das linguagens de programação podem ser *compilados* ou *interpretados*, referindo-se a como as linguagens de programação traduzem o código escrito em sua própria sintaxe ao código da máquina. Resumidamente, linguagens compiladas são aquelas nas quais um programa chamado **compilador** lê o código-fonte, verifica erros e faz otimizações, para então, produzir um arquivo executável do seu programa. Ao termos este executável, não precisamos compilar o código novamente, a não ser que este mude. Linguagens interpretadas possuem um programa chamado **interpretador**, o qual simultaneamente executa e verifica cada linha do código sem necessitar de ser compilado. Neste tipo de abordagem, o código sempre terá de ser interpretado cada vez que executarmos o programa. Se quiser entender um pouco mais a fundo sobre o tema, esta resposta do [Stack Overflow](https://stackoverflow.com/a/38491646/11676927) é um bom começo.

### Não Indentada

Algumas linguagens de programação utilizam indentação como uma característica principal de sua sintaxe. R não utiliza indentação obrigatória.

Indentação é uma forma de se estruturar os comandos de uma linguagem, aumentando sua legibilidade. Basicamente, é um "espaço em branco" na frente do código. Abaixo, há um exemplo de código indentado:

    funcao_exemplo():
        comando1
        comando2
    
    comando3

Vemos claramente que `comando1` e `comando2` pertencem à função `funcao_exemplo()`, pois **está indentada à ela**; há um "espaço em branco" antes de ambos comandos. Por sua vez, `comando3`, não pertence à função, pois estão na **mesma indentação**; não há "espaço em branco" antes de `comando3`. 

Contrariamente, há as linguagens onde a estruturação do código não envolve indentação obrigatória. Exemplo de um código deste tipo:

    funcao_exemplo(){
    comando1
    comando2
    }
    
    comando3
    
Neste exemplo, todo o código está na mesma indentação. Perceba que `comando1` e `comando2` pertencem à `funcao_exemplo()`, mas o que identifica isto são os operadores `{}`. Tudo o que está dentro dos operadores `{}` pertence à função. Deste modo, `comando3`não pertence à função. Para aumentar a legibilidade, poderíamos escrever, opcionalmente:

    funcao_exemplo(){
        comando1
        comando2
    }
    
    comando3

Mas tal forma não é obrigatória.

Em geral, R utiliza os operadores `{}` para agrupar comandos e estruturá-los. Por isto, é uma linguagem onde indentação não é obrigatória.

## CRAN

**CRAN** é um acrônimo do inglês *Comprehensive R Archive Network*. CRAN é um conjunto mundial de servidores que mantêm versões idênticas e atualizadas do R assim como de todos os pacotes disponíveis para utilização. Deste modo, quando queremos instalar R pela primeira vez, geralmente será do CRAN que conseguiremos os arquivos de instalação.

Como repositório principal, é do CRAN que conseguimos instalar qualquer pacote para usarmos no R. Todos os desenvolvedores que desejarem publicar seu pacote em R devem submetê-lo para o CRAN. Após publicado, o pacote é facilmente achado e instalado pelos usuários finais. Você pode acessar o CRAN neste [link](https://cran.r-project.org) e todos os pacotes disponíveis neste [link](https://cran.r-project.org/web/packages/index.html).

## Leia Mais

### Linguagem S

- *The S System* por John Chambers, disponível no [Wayback Machine](https://web.archive.org/web/20181014111802/http://ect.bell-labs.com/sl/S/)

### Linguagem R

- [Site Oficial](https://www.r-project.org/about.html)

- [CRAN](https://cran.r-project.org/doc/FAQ/R-FAQ.html#R-Basics)

- [Ihaka and Gentleman, 1996](https://www.jstor.org/stable/1390807)

### GNU e Softwares Livres

- [GNU - Site Original](https://www.gnu.org/philosophy/free-sw.en.html)

- [GNU - Site em Português](https://www.gnu.org/philosophy/free-sw.html)
