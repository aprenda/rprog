# (PART) Tidyverse {-}

# Introdução {-}

Neste ponto do curso, você já é plenamente capaz de manipular qualquer tipo de dado em R, além de criar suas próprias funções, seja qual for o objetivo. Nesta parte final, aprenderemos a manipular vários pacotes do tidyverse. Basicamente, aprenderemos outras formas mais simples e eficientes de se fazer gráficos e manipular dados em R. Atualmente, a forma "correta" (com grandes aspas), a norma de se trabalhar em R é utilizando o tidyverse. Mas o que é isto?

Tidyverse é **um conjunto de pacotes que ampliam a funcionalidade do R puro**, isto é, diversos pacotes diferentes que modificam a manipulação em R puro e/ou adicionam novas propriedades às mesmas. Especificamente, podemos citar várias características do tidyverse:

1. **É um grupo de pacotes**. Diversos pacotes caem sob a mesma denominação geral *"tidyverse"*. Como veremos, podemos instalar o tidyverse como um pacote, o que instala alguns pacotes principais, mas deixa de fora alguns outros pacotes que também são denominados como parte do tidyverse. Estes têm de serem instalados individualmente. Trataremos disto à frente.
1. **Objetivo comum**. Todos os pacotes do tidyverse são pensados para a ciência de dados. Como sabemos, R é uma linguagem e um software feito por estatísticos para fins estatísticos. Tidyverse em si não faz estatística (ou faz muito pouca), mas nos auxilia muito facilitando todo o processo de preparar, manipular e visualizar os dados.
1. **Unidade**. Todos os pacotes do tidyverse são feitos para trabalharem em conjunto, isto é, são todos compatíveis uns com os outros. Aqui, não há preocupação de se manipular uma estrutura de dados e outro pacote não reconhecer (e quaisquer outros problemas afins). Com tidyverse, trabalhamos de uma única vez com a integração de todos seus diferentes pacotes.

Por tais motivos, tidyverse é a norma de como se trabalhar em R nos dias de hoje.

Outro ponto: como já discutimos em um dos capítulos iniciais, temos de ter um código de estilo para nossos programas, melhorando a legibilidade do código e padronizando-o. Apesar de R não possuir nenhuma convenção geral, tidyverse possui algumas sugestões de código de estilo:

- 80 caracteres por linha;
- Espaços em branco antes e depois dos operadores;
    - Errado: `argumento=FALSE`
    - Correto: `argumento = FALSE`
- Remover espaços em branco no início e fim dos parênteses das funções;
    - Errado: `funcao ( argumento1, argumento2 )`
    - Correto: `funcao(argumento1, argumento2)`
- Nomeação:
    - Utilizar nomes que signifiquem o que está sendo feito;
    - Não reutilizar nomes de variáveis;
    - Evitar acentuação nos nomes;
    - Consistência: se utilizar `_`, utilize somente esta. Se utilizar `camelCase`, utilize somente esta.
- Indentar corretamente o código de acordo com a hierarquia do código;
- Dividir o script em seções.

Sobre o último item: dividimos o script com o comando `Ctrl + Shift + R` (ou veja no seu RStudio como o comando `Insert section` está configurado). O nome de cada seção deve refletir o que está sendo feito naquela parte do código.

## Instalando

Podemos instalar o tidyverse como um todo com o comando:

```{r, eval=FALSE}
install.packages("tidyverse")
```

Este comando instala diversos pacotes do tidyverse de uma única vez. Para carregar este conjunto de pacotes, executamos:

```{r, results="hide"}
library(tidyverse)
```

Contudo, não é uma boa prática carregar os pacotes desta forma. Lembre-se de que tidyverse é um conjunto de pacotes! Se carregamos todos de uma única vez, não estamos sendo explícitos sobre quais pacotes estamos utilizando, além de aumentar o número de conflitos por nomes e máscaras. Por isto, sempre que formos instalar e carregar um pacote, seremos explícitos sobre quais pacotes estaremos utilizando.

Tendo instalado e carregado, podemos ver quais são todos os pacotes do tidyverse com:

```{r, comment=""}
tidyverse_packages()
```

Obviamente, para vermos todos estes pacotes, este livro teria de ser muito maior! Assim, mostraremos aqui somente os principais pacotes.

Trabalhando com tidyverse, utilizaremos a todo momento dois novos elementos: pipes e tibbles. Vejamos ambos na sequência.

## Pipes

*Pipe* (*piping*, *pipe operator*) refere-se ao operador especial `%>%`. Já conhecemos operadores especiais, principalmente o operador `%in%`. Este novo operador representa uma **sequência de comandos** e é feito com o atalho `Ctrl + Shift + M` (ou veja no seu RStudio como o comando `Insert Pipe Operator` está configurado).

O que exatamente é esta sequência de comandos? Vamos fazer uma comparação: pense em como estávamos fazendo um sequência de comandos até agora.

```{r, comment=""}
vetor <- c("R", "Python", "Julia")

vetor_sem_python <- vetor[-2]

vetor_sem_python[3] <- "C++"

print(vetor_sem_python)
```

Perceba a sequência acima:

1. Criamos um vetor;
1. Retiramos o segundo elemento (`"Python"`);
1. Adicionamos a string `"C++"` à última posição;
1. Print do vetor final.

Como daria-se esta sequência com o operador `%>%` em tidyverse? Veja:

```{r, comment=""}
vetor <- c("R", "Python", "Julia")

vetor %>%
    .[-2] %>%
    c(., "C++")
```

> Nota: veja a indentação do código abaixo de `vetor %>%`, significando que todos os comandos abaixo utilizam este conjunto de dados.

Perceba que:

1. Criamos o vetor de interesse;
1. Acessamos este vetor;
1. Retiramos o segundo elemento (`"Python"`);
1. Adicionamos a string `"C++"` à última posição;
1. O resultado da última expressão avaliada é retornada no console.

Este é o objetivo do pipe: organizar e facilitar a sequência de comandos feita. Atente-se a algumas características do código com *piping*: i) **não há necessidade da criação de variáveis**: fazemos todos os comandos sem necessidade de gravar o resultado anterior na memória, pois é uma sequência de comandos — o comando posterior utilizará o dado (ou conjunto de dados) da forma que foi modificada no passo anterior; ii) **podemos criar/remover passos intermediários com facilidade**: basta adicionar um comando anterior ao desejado ou comentar um comando da sequência (e este não será executado); iii) **conseguimos criar um código de forma dinâmica**: com esta sintaxe, conseguimos manipular um conjunto de dados da forma que quisermos e, no fim, da sequência basta utilizarmos o conjunto de dados pronto como argumento em uma função ou para fazer um gráfico.

Resumindo: *piping* avalia uma expressão e utiliza seu retorno como entrada para a próxima expressão da sequência.

### Salvando Variáveis

Perceba que o código não é salvo ao final com *piping*; o conjunto de dados original não foi modificado:

```{r, comment=""}
vetor <- c("R", "Python", "Julia")

vetor %>%
    .[-2] %>%
    c(., "C++")

print(vetor)
```

Com *piping*, modificamos como quisermos um conjunto de dados, mas este não é salvo por default. Se quisermos salvar o resultado de toda uma sequência de dados, precisamos criar explicitamente uma variável. A atribuição da variável vem no início da sequncia, como mostrado abaixo:

```{r, comment=""}
vetor <- c("R", "Python", "Julia")

# Modificando o objeto e criando uma nova variável:
vetor_com_cpp <- vetor %>%
    .[-2] %>%
    c(., "C++")

print(vetor_com_cpp)
```

## Non-Standard Evaluation

Um detalhe: veja, no código que fizemos anteriormente, o uso do operador `.`, como em `.[-2]` ou `c(., "C++")`. Este operador, em tidyverse, é dito *dot notation* (notação "ponto"/"ponto final"). Ele representa (geralmente) uma **variável especial que será avaliada segundo a função em uso**; um argumento *dummy*. Veja nosso exemplo:

```{r, eval=FALSE}
vetor <- c("R", "Python", "Julia")

vetor %>%
    .[-2] %>%
    c(., "C++")
```

Agora, vamos nos lembrar de que `[` é uma função: estamos fazendo uma indexação; pegando um subconjunto de algo (veja a página de ajuda do operador, se necessitar).

Em `.[-2]`, o operador `.` está representando o argumento da função `[`, ou seja, o objeto sendo indexado. Como o objeto que estamos lidando é `vetor` (passo anterior da sequência), é este objeto que `.` representa.

Já na execução posterior: `c(., "C++")`, o operador `.` representa um argumento da função `c()`. A função `c()` concatena dois objetos, no caso, `.` e `"C++"`. Aqui, `.` representa o vetor `c("R", "Julia")`, o qual foi criado no passo anterior da sequência.

Um outro exemplo. Suponha que tenhamos a seguinte função:

```{r, eval=FALSE}
funcao(x, y, z)
```

Com este operador, podemos escolher em qual argumento os dados serão fornecidos. Exemplo:

```{r, eval=FALSE}
# Utilizando 'conjunto_dados' no argumento 'x':
conjunto_dados %>%
    funcao(., y, z)

# Utilizando 'conjunto_dados' no argumento 'y':
conjunto_dados %>%
    funcao(x, ., z)

# Utilizando 'conjunto_dados' no argumento 'z':
conjunto_dados %>%
    funcao(x, y, .)
```

Em outras palavras: o operador `.` contém o resultado do *piping* anterior (passo anterior da sequência).

Uma outra notação importante é a do operador `~`. Sabemos que os operadores têm funções diferentes de acordo com o contexto em que estão sendo executados. Em R puro, o operador `~` significa uma fórmula (onde o operando esquerdo é a variável dependente e o direito é a variável independente). Em tidyverse, em conjunção com `.`, ele tem outra conotação: significa uma função anônima, a qual já vimos e sabemos como fazer.

Em geral, em tidyverse, a conjunção `~.` é uma **avaliação não padrão** (*non-standard evaluation; NSE*). Isto é: `~` representa uma função anônima com qualquer número de argumentos `function(...)` e o operador `.`, indica um argumento desta função.

Veja um exemplo com a função `detect()`, a qual retorna o valor ou posição do resultado de uma expressão:

```{r, comment=""}
numeros <- c(32, 33, 5, 52, 57)

# Quais valores menores do que 10?
numeros %>%
    detect(~.x < 10)
```

Acima, `~.x` representa uma função anônima com argumento `.x`. O código abaixo é idêntico em funcionalidade, só muda a sintaxe:

```{r, comment=""}
numeros <- c(32, 33, 5, 52, 57)

# Quais valores menores do que 10?
numeros %>%
    detect(function(...){..1 < 10})
```

Onde `..1` representa o primeiro argumento da função anônima.

Neste exemplo, usamos `detect()`. Se utilizar outra função e você fornecer mais argumentos, o operador `~` atribuirá cada argumento passado aos parâmetros da função, respectivamente.

> Nota: tenha em mente que estamos falando de tidyverse! Os argumentos `~.` em R puro tem outro significado!

## Tibbles

Um novidade com a qual trabalharemos a partir de agora são os **tibbles** — uma "nova" estrutura de dados. Um tibble nada mais é do que uma atualização dos data frames do R.

A linguagem R é antiga, uma linguagem da década de 70. Uma de suas estruturas fundamentais — o data frame — possuiu atualizações ao longo do tempo, mas sua base é praticamente é a mesma. Uma grande mudança nesta estrutura provocaria incompatibilidades e quebra de muitos programas, principalmente os antigos. Além disto, algumas característica dos data frames que eram úteis antigamente não o são atualmente. Por estes motivos, foi criado o pacote `tibble`, o qual disponibiliza estas estruturas modernas e mais eficientes, contendo as partes boas dos data frames e removendo as ruins.

Quando instalamos e carregamos o pacote `tidyverse` no início deste capítulo, o pacote `tibble` foi também carregado. Vamos aqui mostrar as funcionalidades básicas deste objeto.

> Nota: se você quiser ler mais sobre tibble, execute o código `vignette("tibble")`.

Primeiramente, vamos criar um data frame:

```{r, comment=""}
alunos <- data.frame(
    Nome = c(
        "Nathália", "João", "Marcos", "Olívia",
        "Rhayane", "Bruna", "Danira", "Paulo",
        "Leandro", "Nicholas", "Luiza", "Joyce",
        "Tiago", "Daniel", "Paula", "Nicole",
        "Rogério", "Natan", "José", "Maria Clara"
    ),
    Matricula = c(
        23001, 23002, 23003, 23004, 23005,
        23006, 23007, 23008, 23009, 23010,
        23011, 23012, 23013, 23014, 23015,
        23016, 23017, 23018, 23019, 23020
    ),
    Cadastro = c(
        "Completo", "Incompleto", "Incompleto", "Completo",
        "Incompleto", "Completo", "Completo", "Completo",
        "Incompleto", "Incompleto", "Completo", "Completo",
        "Completo", "Completo", "Completo", "Completo",
        "Completo", "Incompleto", "Incompleto", "Incompleto"
    )
)

print(alunos)
```

Temos um data frame normal, como podemos ver:

```{r, comment=""}
class(alunos)
```

Vamos transformar em tibble com a função `as_tibble()`, que coage um objeto data frame para tibble:

```{r, comment=""}
alunos <- as_tibble(alunos)
```

Vejamos a mudança de classe:

```{r, comment=""}
class(alunos)
```

O objeto `alunos` agora possui as classes próprias de tibble (`tbl_df`, `tbl`).

Façamos seu print:

```{r, comment=""}
print(alunos)
```

Esta é uma das principais diferenças entre tibbles e data frames: como é feito o print. Em geral, data frames mostrarão todo o conteúdo do data frame (todas as linhas e colunas), enquanto tibbles mostram apenas uma pequena parte inicial — as primeiras 10 linhas e quantas colunas couberem na tela.

Podemos criar tibbles diretamente com a função `tibble()`:

```{r, comment=""}
alunos <- tibble(
    Nome = c(
        "Nathália", "João", "Marcos", "Olívia",
        "Rhayane", "Bruna", "Danira", "Paulo",
        "Leandro", "Nicholas", "Luiza", "Joyce",
        "Tiago", "Daniel", "Paula", "Nicole",
        "Rogério", "Natan", "José", "Maria Clara"
    ),
    Matricula = c(
        23001, 23002, 23003, 23004, 23005,
        23006, 23007, 23008, 23009, 23010,
        23011, 23012, 23013, 23014, 23015,
        23016, 23017, 23018, 23019, 23020
    ),
    Cadastro = c(
        "Completo", "Incompleto", "Incompleto", "Completo",
        "Incompleto", "Completo", "Completo", "Completo",
        "Incompleto", "Incompleto", "Completo", "Completo",
        "Completo", "Completo", "Completo", "Completo",
        "Completo", "Incompleto", "Incompleto", "Incompleto"
    )
)

print(alunos)
```

Uma função importante para utilizarmos em tidyverse é `glimpse()`. Em data frames, utilizávamos `str()` para vermos a estrutura do conjunto de dados. Em tidyverse, usamos `glimpse()` para vermos este resumo do conjunto de dados. Exemplo:

```{r, comment=""}
glimpse(alunos)
```

Vimos que `alunos` é um tibble de 20x3 e possui as seguintes colunas: `Nome` (caracteres), `Matricula` (numérica) e `Cadastro` (caracteres).

## Leia Mais

### Tidyverse {-}

- [Página Oficial](https://www.tidyverse.org)
- [Página Oficial do Pacote](https://tidyverse.tidyverse.org)
- [Código de Estilo](https://style.tidyverse.org)
- [Princípios de Design](https://design.tidyverse.org/index.html)