# Repeat

O loop `repeat()` é uma outra forma de iteração por condição em R. Esta estrutura itera sobre um bloco de código e encerra somente quando uma condição tornar-se **verdadeira**. Esteja atento:

- O loop `while` continua iterando enquanto uma condição for verdadeira.
- O loop `repeat` continua iterando enquanto uma condição for falsa.

Sua sintaxe básica é:

```{r, eval=FALSE}
repeat{
    bloco
}
```

ou (sem as chaves):

```{r, eval=FALSE}
repeat bloco
```

Onde `bloco` é o bloco de código a ser executado repetidamente.

Se executarmos um código assim, claramente faremos um loop infinito. Temos então que adicionar uma condição de parada para o loop. Vamos então melhorar nesta sintaxe adicionando uma condicional:

```{r, eval=FALSE}
repeat{
    
    bloco
    
    if(condição){
        break
    }
}
```

A sintaxe acima é bem mais clara em como o loop `repeat` ocorre: as iterações ocorrerão até o momento em que uma certa condição é satisfeita, e então, o `break` ativado, encerrando o loop.

## Criando loops `repeat`

Vamos colocar em prática o que acabamos de ver, iniciando por um loop extremamente simples: vamos printar os números de `1` a `10`.

```{r, comment=""}
# Variável auxiliar:
numero <- 0

# Loop:
repeat{
    
    # Print da variável:
    print(numero)
    
    # Atualização:
    numero <- numero + 1
    
    # Encerramento:
    if(numero > 10) break
}
```

No código acima, vemos que o loop `repeat` compartilha algumas características com o loop `while`. Primeiramente, criamos uma variável auxiliar `numero` para controlar o loop, a utilizando como condição de parada na condicional dentro do loop (`if(numero > 10)`). Se o número for maior que `10`, o `break` é ativado e o loop `repeat` encerrado. Também, dentro do loop, temos de atualizar a variável auxiliar. Caso contrário, será novamente um loop infinito, printando somente o valor `0` para sempre.

Outro exemplo: vamos printar os 100 primeiros múltiplos de 4.

```{r, comment=""}
# Variáveis auxiliares:
qtd_multiplos <- 0

numero <- 1

# Loop:
repeat{
    
    # Condicional do print:
    if(!(numero %% 4)){
        
        # Print da variável:
        print(numero)
        
        # Atualização dos múltiplos:
        qtd_multiplos <- qtd_multiplos + 1
    }
    
    # Atualização da variável:
    numero <- numero + 1
    
    # Encerramento:
    if(qtd_multiplos > 100) break
}
```

## Exercícios

::: {.infobox .exclamacao data-latex="{exclamacao}"}
**Pesquise!**<br><br>
Uma das características de um programador é saber procurar respostas para os problemas em seu código e afins. Não sabe como responder alguma das questões? Pesquise sobre o tema em questão (na internet, na documentação oficial, ...) e aprenda mais sobre ele!
:::

### Questão 1 {-}

Crie um loop `repeat` que faça o print dos números ímpares de 1 a 1000.

### Questão 2 {-}

Crie um loop `repeat` que faça o print dos números pares e divisíveis por 8 de 1 a 1000.

### Questão 3 {-}

Faça a tabuada do 5 com o loop `repeat`.

### Questão 4 {-}

Faça a tabuada de 1 a 10 com o loop `repeat`.

> Nota: não faça 10 loops diferentes.

### Questão 5 {-}

Faça uma iteração com o loop `repeat` sobre os valores pares de 500 números aleatórios entre `0` e `1000`.

### Questão 6 {-}

Faça uma iteração com o loop `repeat` sobre os valores ímpares e múltiplos de 7 de 500 números aleatórios entre `0` e `1000`.

### Questão 7 {-}

Crie a figura abaixo com o loop `repeat`.

```{r ex_repeat_loop, echo=FALSE, fig.align = "center"}
knitr::include_graphics("img/ex_for.png")
```

### Questão 8 {-}

Crie o padrão abaixo com o loop `repeat`.

```{r ex_repeat_loop2, echo=FALSE, fig.align = "center"}
knitr::include_graphics("img/ex_for2.png")
```

### Questão 9 {-}

Faça o print dos primeiros 20 números pares (incluindo zero e em ordem inversa) com o loop `repeat`.

## Respostas

### Questão 1 {-}

```{r, eval=FALSE}
numero <- 1

repeat{
    
    if((numero %% 2) != 0) print(numero)
    
    numero <- numero + 1
    
    if(numero > 1000) break
}
```

### Questão 2 {-}

```{r, eval=FALSE}
numero <- 1

repeat{
    
    # Todos múltiplos de 8 são múltiplos de 2, então:
    if(!(numero %% 8)) print(numero)
    
    numero <- numero + 1
    
    if(numero > 1000) break
}
```

### Questão 3 {-}

```{r, eval=FALSE}
numero <- 0

repeat{
    
    print(paste("5*", numero, " = ", 5 * numero, sep=""))
    
    numero <- numero + 1
    
    if(numero > 10) break
}
```

### Questão 4 {-}

```{r, eval=FALSE}
fator_externo <- 1

repeat{
    
    fator_interno <- 0
    
    repeat{
        
        print(paste(fator_externo, "*", fator_interno, " = ", fator_externo*fator_interno, sep=""))
        
        fator_interno <- fator_interno + 1
        
        if(fator_interno > 10) break
    }
    
    fator_externo <- fator_externo + 1
        
    if(fator_externo > 10) break else print("----------------------------")
}
```

### Questão 5 {-}

```{r, eval=FALSE}
idx <- 1

numeros <- sample(0:1000, 500)

repeat{
    
    if(!(numeros[idx] %% 2)) print(numeros[idx])
    
    idx <- idx + 1
    
    if(idx > length(numeros)) break
}
```

### Questão 6 {-}

```{r, eval=FALSE}
idx <- 1

numeros <- sample(0:1000, 500)

repeat{
    
    numero_atual <- numeros[idx]
    
    if((numero_atual %% 2 != 0) && !(numero_atual %% 7))
        print(numero_atual)
    
    idx <- idx + 1
    
    if(idx > length(numeros)) break
}
```

### Questão 7 {-}

```{r, eval=FALSE}
numero <- 1

repeat{
    
    print(rep("*", numero))
    
    numero <- numero + 1
    
    if(numero > 10) break
}
```

### Questão 8 {-}

```{r, eval=FALSE}
produto <- c("Leite", "Queijo")

animais <- c("Vaca", "Cabra", "Ovelha", "Búfala")

idx_produto <- 1

repeat{
    
    idx_animal <- 1
    
    repeat{
        
        print(paste(produto[idx_produto], "de", animais[idx_animal]))
        
        idx_animal <- idx_animal + 1
        
        if(idx_animal > length(animais)) break
    }
    
    idx_produto <- idx_produto + 1
        
    if(idx_produto > length(produto)) break
}
```

### Questão 9 {-}

```{r, eval=FALSE}
qtd <- 0

# Primeiros 100 numeros
numeros <- 0:100

# Números pares:
numeros_pares <- numeros[!(numeros %% 2)]

# Primeiros 20:
numeros_pares <- numeros_pares[20:1]

idx <- 1

repeat{
    
    print(numeros_pares[idx])
    
    idx <- idx + 1
    
    if(idx > length(numeros_pares)) break
}
```